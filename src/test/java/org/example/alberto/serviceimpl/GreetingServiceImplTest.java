package org.example.alberto.serviceimpl;

import org.example.alberto.model.GreetingDao;
import org.example.alberto.service.GreetingService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
class GreetingServiceImplTest {

    @TestConfiguration
    static class GreetingServiceImplTestContextConfiguration {
        @Bean
        public GreetingService greetingService(){
            return new GreetingServiceImpl() {
            };
        }
    }

    @Autowired
    private GreetingService greetingService;

    @Test
    void getGreetingTest(){

        GreetingDao greetingDaoToRemove = greetingService.getGreeting("me");

        assertEquals(1, greetingDaoToRemove.getId());
        assertEquals("Hello, me!", greetingDaoToRemove.getContent());

    }

}
