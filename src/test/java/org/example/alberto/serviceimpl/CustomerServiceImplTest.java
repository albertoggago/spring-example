package org.example.alberto.serviceimpl;

import jakarta.validation.ConstraintViolationException;
import org.bson.types.ObjectId;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.exception.BadDataException;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.repository.CustomerRepository;
import org.example.alberto.service.AuditLogService;
import org.example.alberto.transformer.CustomerMock;
import org.hibernate.exception.JDBCConnectionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {

    private static final String NEW_NAME = "new name";
    private static final String NEW_EMAIL = "newEmail@gmail.com";
    private static final String BAD_EMAIL = "BAD EMAIL";


    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    AuditLogService auditLogService;

    CustomerEntity customerEntity;

    CustomerDao customerDao;

    @BeforeEach
    void setup() {
        customerEntity = CustomerMock.newCustomerEntity();
        customerDao = CustomerMock.newCustomerDao();

        AuditLogEntity auditLogEntity = new AuditLogEntity();
        auditLogEntity.setId(new ObjectId("5eea9c7a5a0c247850761cdb"));
        when(auditLogService.saveNew(any(), any(), any(), any())).thenReturn(auditLogEntity);

    }

    @Test
    void getCustomersTest() throws EntityNotFoundException {

        Page<CustomerEntity> page = new PageImpl<>(Collections.singletonList(customerEntity));
        when(customerRepository.findAll((Specification<CustomerEntity>) any(), (Pageable) any())).thenReturn(page);

        List<CustomerDao> customers = customerServiceImpl.list(0, 1, "", "");
        assertEquals(1, customers.size());
        assertEquals(CustomerMock.ALIAS, customers.getFirst().getAlias());
        assertEquals(CustomerMock.NAME, customers.getFirst().getName());
        assertEquals(CustomerMock.EMAIL, customers.getFirst().getEmail());
    }

    @Test
    void getCustomersTestSortingError() {

        CustomerEntity customerEntity1 = CustomerMock.newCustomerEntity();
        customerEntity1.setAlias("2 Two");
        CustomerEntity customerEntity2 = CustomerMock.newCustomerEntity();
        customerEntity2.setAlias("1 One");
        Page<CustomerEntity> page = new PageImpl<>(Arrays.asList(customerEntity1, customerEntity2));
        when(customerRepository.findAll((Specification<CustomerEntity>) any(), (Pageable) any())).thenReturn(page);

        IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
                () -> customerServiceImpl.list(0, 1, "address asc", ""));
        assertEquals("Sort element does not exist: address", error.getMessage());
    }

    @Test
    void getCustomersTestError() {

        Page<CustomerEntity> page = new PageImpl<>(new ArrayList<>());
        when(customerRepository.findAll((Specification<CustomerEntity>) any(), (Pageable) any())).thenReturn(page);

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        customerServiceImpl.list(0, 1, "", "");
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.CUSTOMER));
    }

    @Test
    void getOneCustomerTest() {

        customerEntity.setId(1L);
        when(customerRepository.findByAlias(CustomerMock.ALIAS)).thenReturn(customerEntity);

        try {
            CustomerDao customer = customerServiceImpl.findCustomerByAlias(CustomerMock.ALIAS);
            assertEquals(CustomerMock.ALIAS, customer.getAlias());
            assertEquals(CustomerMock.NAME, customer.getName());
            assertEquals(CustomerMock.EMAIL, customer.getEmail());

        } catch (EntityNotFoundException e) {
            fail();
        }

    }

    @Test
    void getOneCustomerError() {

        customerEntity.setId(1L);
        when(customerRepository.findByAlias(CustomerMock.ALIAS)).thenReturn(customerEntity);
        when(customerRepository.findByAlias(StandardTexts.NOTHING)).thenReturn(new CustomerEntity());

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        customerServiceImpl.findCustomerByAlias(StandardTexts.NOTHING);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.CUSTOMER));
    }

    @Test
    void getOneCustomerErrorNull() {

        customerEntity.setId(1L);
        when(customerRepository.findByAlias(CustomerMock.ALIAS)).thenReturn(customerEntity);
        when(customerRepository.findByAlias(StandardTexts.NOTHING)).thenReturn(null);

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        customerServiceImpl.findCustomerByAlias(StandardTexts.NOTHING);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.CUSTOMER));
    }
    @Test
    void createCustomerOK() {
        CustomerDao customerNew = CustomerMock.newCustomerDao();
        customerNew.setId(null);
        customerNew.setCreationDate(null);
        customerNew.setUpdatedDate(null);
        when(customerRepository.save(any())).thenReturn(customerEntity);

        try {
            CustomerDao customer = customerServiceImpl.createCustomer(customerNew);
            assertEquals(CustomerMock.ALIAS, customer.getAlias());
            assertEquals(CustomerMock.NAME, customer.getName());
            assertEquals(CustomerMock.EMAIL, customer.getEmail());

        } catch (BadDataException | JDBCConnectionException e) {
            fail();
        }
    }

    @Test
    void createCustomerNull() {

        BadDataException badDataException = assertThrows(
                BadDataException.class,
                () -> {
                    try {
                        customerServiceImpl.createCustomer(null);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(badDataException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
        assertTrue(badDataException.getMessage().contains(StandardTexts.CUSTOMER));
    }

    @Test
    void createCustomerEmailWrong() {
        CustomerDao customerNew = CustomerMock.newCustomerDao();
        customerNew.setId(null);
        customerNew.setCreationDate(null);
        customerNew.setUpdatedDate(null);
        customerNew.setEmail(BAD_EMAIL);
        ConstraintViolationException constraintViolationException = new ConstraintViolationException("BAD EMAIL", null);
        when(customerRepository.save(any())).thenThrow(constraintViolationException);

        BadDataException badDataException = assertThrows(
                BadDataException.class,
                () -> {
                    try {
                        customerServiceImpl.createCustomer(customerNew);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(badDataException.getMessage().contains(StandardTexts.SOME_ELEMENT_IS_NOT_VALID));
    }

    @Test
    void updatedCustomerOK() {

        customerDao.setName(NEW_NAME);
        customerDao.setEmail(NEW_EMAIL);
        customerEntity.setId(1L);
        when(customerRepository.findByAlias(customerDao.getAlias())).thenReturn(customerEntity);

        try {
            CustomerDao customer = customerServiceImpl.updatedCustomer(customerDao);
            assertEquals(NEW_NAME, customer.getName());
            assertEquals(NEW_EMAIL, customer.getEmail());

        } catch (BadDataException | JDBCConnectionException | EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    void updatedCustomerNull() {

        BadDataException badDataException = assertThrows(
                BadDataException.class,
                () -> {
                    try {
                        customerServiceImpl.updatedCustomer(null);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(badDataException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
        assertTrue(badDataException.getMessage().contains(StandardTexts.CUSTOMER));
    }

    @Test
    void updatedCustomerDoNotExistNull() {

        customerDao.setName(NEW_NAME);
        customerDao.setEmail(NEW_EMAIL);

        when(customerRepository.findByAlias(customerDao.getAlias())).thenReturn(null);

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        customerServiceImpl.updatedCustomer(customerDao);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    @Test
    void updatedCustomerErrorUpdate() {

        customerDao.setName(NEW_NAME);
        customerDao.setEmail(NEW_EMAIL);
        customerEntity.setId(1L);
        when(customerRepository.findByAlias(customerDao.getAlias())).thenReturn(customerEntity);
        when(customerRepository.save(any())).thenThrow(new ConstraintViolationException(StandardTexts.DO_NOT_EXIST, null));

        BadDataException badDataException = assertThrows(
                BadDataException.class,
                () -> {
                    try {
                        customerServiceImpl.updatedCustomer(customerDao);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(badDataException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    @Test
    void deleteAllTest() {

        Page<CustomerEntity> page = new PageImpl<>(Collections.singletonList(customerEntity));
        when(customerRepository.findAll(any(PageRequest.class))).thenReturn(page);
        try {
            customerServiceImpl.deleteAllCustomer();
        } catch (EntityNotFoundException e) {
            fail();
        }

        verify(customerRepository, times(1)).deleteAll();
    }

    @Test
    void deleteAllErrorTest() {

        Page<CustomerEntity> page = new PageImpl<>(new ArrayList<>());
        when(customerRepository.findAll(any(PageRequest.class))).thenReturn(page);
        when(customerRepository.findAll()).thenReturn(new ArrayList<>());
        try {
            customerServiceImpl.deleteAllCustomer();
            fail();
        } catch (EntityNotFoundException e) {
            verify(customerRepository, times(0)).deleteAll();
        }
    }

    @Test
    void deleteOneTest() throws EntityNotFoundException {

        customerEntity.setId(1L);
        when(customerRepository.findByAlias(any())).thenReturn(customerEntity);

        customerServiceImpl.deleteOneCustomer(customerDao.getAlias());

        verify(customerRepository, times(1)).deleteById(any());
    }

    @Test
    void deleteOneErrorTest() {

        when(customerRepository.findByAlias(any())).thenReturn(null);

        try {
            customerServiceImpl.deleteOneCustomer(customerDao.getAlias());
            fail();
        } catch (EntityNotFoundException e) {
            verify(customerRepository, times(0)).deleteById(any());
        }
    }

    @Test
    void getCustomersTestSorting() throws EntityNotFoundException {

        CustomerEntity customerEntity1 = CustomerMock.newCustomerEntity();
        customerEntity1.setAlias("2 Two");
        CustomerEntity customerEntity2 = CustomerMock.newCustomerEntity();
        customerEntity2.setAlias("1 One");
        Page<CustomerEntity> page = new PageImpl<>(Arrays.asList(customerEntity1, customerEntity2));
        when(customerRepository.findAll((Specification<CustomerEntity>) any(), (Pageable) any())).thenReturn(page);

        List<CustomerDao> customers =
                customerServiceImpl.list(0, 1, "creationDate asc, alias asc", "");
        assertEquals(2, customers.size());
        assertTrue(customers.getFirst().getAlias().compareTo(customers.getLast().getAlias()) > 0);
    }

}