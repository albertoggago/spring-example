package org.example.alberto.serviceimpl;

import org.bson.types.ObjectId;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.transformer.AuditLogMock;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.AuditLogDao;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.example.alberto.mongdb.AuditLogRepository;
import org.example.alberto.mongdb.StatisticsRepository;
import org.example.alberto.service.PersonalConfigurationService;
import org.example.alberto.transformer.CustomerMock;
import org.hibernate.exception.JDBCConnectionException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class AuditLogServiceImplTest {

    @InjectMocks
    AuditLogServiceImpl auditLogServiceImpl;

    @Mock
    AuditLogRepository auditLogRepository;

    @Mock
    StatisticsRepository statisticsRepository;

    @Mock
    PersonalConfigurationService personalConfigurationService;

    AuditLogEntity auditLogEntity;

    @BeforeEach
    void setUp() {
        Optional<AuditLogEntity> auditLogEntityOptional = Optional.of(new AuditLogEntity());
        when(auditLogRepository.findById(any())).thenReturn(auditLogEntityOptional);
        auditLogEntity = AuditLogMock.newAuditLogEntity();

        doAnswer(invocationOnMock -> {
            AuditLogEntity auditLogEntityNew = invocationOnMock.getArgument(0);
            auditLogEntityNew.setId(new ObjectId(AuditLogMock.ID_AUDIT_LOG));
            return null;
        })
                .when(auditLogRepository).save(any());
        auditLogEntity = auditLogServiceImpl.saveNew(AuditLogMock.ACTION, AuditLogMock.TRACE.getFirst(),
                                                     AuditLogMock.PROPERTIES, CustomerMock.newCustomerDao());
    }

    @Test
    void saveNewTest() {

        when(personalConfigurationService.getTestMode()).thenReturn(true);

        AuditLogEntity auditLogEntity2 = auditLogServiceImpl.saveNew(AuditLogMock.ACTION,
                                                                    AuditLogMock.TRACE.getFirst(),
                                                                    AuditLogMock.PROPERTIES,
                                                                    CustomerMock.newCustomerDao());

        assertEquals(AuditLogMock.ACTION, auditLogEntity2.getAction());
        assertTrue(auditLogEntity.getTrace().contains(AuditLogMock.TRACE.getFirst()));
        assertEquals(AuditLogMock.PROPERTIES.get("param1"), auditLogEntity.getParameters().get("param1"));
        assertNotNull(auditLogEntity.getCreatedDate());

        verify(auditLogRepository, times(1)).findById(any());
        verify(auditLogRepository, times(2)).save(any());
    }

    @Test
    void saveNewNoDebugTest() {

        when(personalConfigurationService.getTestMode()).thenReturn(false);

        AuditLogEntity auditLogEntity2 = auditLogServiceImpl.saveNew(AuditLogMock.ACTION,
                AuditLogMock.TRACE.getFirst(), AuditLogMock.PROPERTIES, CustomerMock.newCustomerDao());

        verify(auditLogRepository, times(0)).findById(any());
        assertEquals(auditLogEntity.getId(), auditLogEntity2.getId());
    }

    @Test
    void saveNewVoidTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(true);

        AuditLogEntity auditLogEntityNew = auditLogServiceImpl.saveNew(null, null, null, null);

        assertNull(auditLogEntityNew.getAction());
        assertEquals(0, auditLogEntityNew.getTrace().size());
        assertEquals(0, auditLogEntityNew.getParameters().size());
        assertNotNull(auditLogEntityNew.getCreatedDate());

        verify(auditLogRepository, times(1)).findById(any());
        verify(auditLogRepository, times(2)).save(any());
    }

    @Test
    void saveTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(true);

        auditLogServiceImpl.save(auditLogEntity, "trace2");

        assertEquals(AuditLogMock.ACTION, auditLogEntity.getAction());
        assertTrue(auditLogEntity.getTrace().contains(AuditLogMock.TRACE.getFirst()));
        assertTrue(auditLogEntity.getTrace().contains("trace2"));
        assertEquals("value1", auditLogEntity.getParameters().get("param1"));
        assertNotNull(auditLogEntity.getCreatedDate());
        assertNotNull(auditLogEntity.getUpdatedDate());

        verify(auditLogRepository, times(1)).findById(any());
        verify(auditLogRepository, times(2)).save(any());
    }

    @Test
    void saveNoDebugTestTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(false);

        auditLogServiceImpl.save(auditLogEntity, "trace2");

        verify(auditLogRepository, times(0)).findById(any());
    }

    @Test
    void saveNullTest() {

        auditLogServiceImpl.save(null, "trace2");

        verify(auditLogRepository,times(1)).save(any());
    }

    @Test
    void saveErrorTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(true);

        auditLogServiceImpl.saveError(auditLogEntity, "error", new ArrayList<>(Arrays.asList("error 1", "error 2")));

        assertEquals(AuditLogMock.ACTION, auditLogEntity.getAction());
        assertEquals("error", auditLogEntity.getError());
        assertNotNull(auditLogEntity.getCreatedDate());
        assertNotNull(auditLogEntity.getEndDate());
        assertNotNull(auditLogEntity.getTraceError());

        verify(auditLogRepository, times(1)).findById(any());
        verify(auditLogRepository, times(2)).save(any());
    }

    @Test
    void saveErrorNoDebugTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(false);

        auditLogServiceImpl.saveError(auditLogEntity, "error", new ArrayList<>(Arrays.asList("error 1", "error 2")));

        verify(auditLogRepository, times(0)).findById(any());
    }

    @Test
    void saveErrorNullTest() {

        auditLogServiceImpl.saveError(null, "trace2", null);

        verify(auditLogRepository, times(1)).save(any());
    }

    @Test
    void saveAndTerminateTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(true);

        auditLogServiceImpl.saveAndTerminate(auditLogEntity, CustomerMock.newCustomerEntity());

        assertEquals(AuditLogMock.ACTION, auditLogEntity.getAction());
        assertNotNull(auditLogEntity.getCreatedDate());
        assertNotNull(auditLogEntity.getEndDate());
        assertNotNull(auditLogEntity.getOutputObject());

        verify(auditLogRepository, times(1)).findById(any());
        verify(auditLogRepository, times(2)).save(any());

    }

    @Test
    void saveAndTerminateNoDebugTest() {
        when(personalConfigurationService.getTestMode()).thenReturn(false);

        auditLogServiceImpl.saveAndTerminate(auditLogEntity, CustomerMock.newCustomerEntity());

        verify(auditLogRepository, times(0)).findById(any());
    }

    @Test
    void saveAndTerminateNullTest() {

        auditLogServiceImpl.saveAndTerminate(null, CustomerMock.newCustomerEntity());

        verify(auditLogRepository, times(1)).save(any());
    }

    @Test
    void findToEntityTest() {

        when(personalConfigurationService.getTestMode()).thenReturn(false);

        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        auditLogEntityNew.setId(new ObjectId(AuditLogMock.ID_AUDIT_LOG));
        Optional<AuditLogEntity> auditLogEntityOptional = Optional.of(auditLogEntityNew);

        when(auditLogRepository.findById(AuditLogMock.ID_AUDIT_LOG)).thenReturn(auditLogEntityOptional);

        AuditLogEntity auditLogEntityReturn = auditLogServiceImpl.findEntityById(AuditLogMock.ID_AUDIT_LOG);

        assertEquals(AuditLogMock.ID_AUDIT_LOG, auditLogEntityReturn.getId().toHexString());

        verify(auditLogRepository, times(1)).findById(any());
    }

    @Test
    void findToDaoTest() {

        when(personalConfigurationService.getTestMode()).thenReturn(false);

        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        auditLogEntityNew.setId(new ObjectId(AuditLogMock.ID_AUDIT_LOG));
        Optional<AuditLogEntity> auditLogEntityOptional = Optional.of(auditLogEntityNew);

        when(auditLogRepository.findById(AuditLogMock.ID_AUDIT_LOG)).thenReturn(auditLogEntityOptional);

        try {
            AuditLogDao auditLogDaoReturn = auditLogServiceImpl.findDaoById(AuditLogMock.ID_AUDIT_LOG);
            assertEquals(AuditLogMock.ID_AUDIT_LOG, auditLogDaoReturn.getId());
            verify(auditLogRepository, times(1)).findById(any());
        } catch (EntityNotFoundException e) {
            fail();
        }

    }

    @Test
    void findToDaoErrorTest() {

        when(personalConfigurationService.getTestMode()).thenReturn(false);

        Optional<AuditLogEntity> auditLogEntityOptional = Optional.empty();
        when(auditLogRepository.findById(AuditLogMock.ID_AUDIT_LOG)).thenReturn(auditLogEntityOptional);

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        auditLogServiceImpl.findDaoById(AuditLogMock.ID_AUDIT_LOG);
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));

    }

    @Test
    void listTest() {
        when(auditLogRepository
                .findAllNew(anyLong(), anyLong(), anyString(), anyString()))
                .thenReturn(Collections.singletonList(auditLogEntity));

        try {
            List<AuditLogDao> auditLogEntities = auditLogServiceImpl.list(0, 1, "", "");
            assertEquals(1, auditLogEntities.size());
            assertNotNull(auditLogEntities.getFirst().getId());
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    void listNullTest() {
        Page<AuditLogEntity> page = new PageImpl<>(new ArrayList<>());
        when(auditLogRepository.findAll((Pageable) any())).thenReturn(page);

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        auditLogServiceImpl.list(0, 1, "", "");
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    @Test
    void listStatisticsTest() {
        AuditLogStatisticsDao auditLogStatisticsDao = createAuditLogStatisticsOutput();

        when(auditLogRepository.statisticsAuditLog()).thenReturn(Collections.singletonList(auditLogStatisticsDao));

        try {
            List<AuditLogStatisticsDao> auditLogStatisticsDaoList = auditLogServiceImpl.findStatistics();
            assertEquals(1, auditLogStatisticsDaoList.size());
            assertEquals(auditLogStatisticsDao, auditLogStatisticsDaoList.getFirst());
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    void listStatisticsNullTest() {
        when(auditLogRepository.statisticsAuditLog()).thenReturn(new ArrayList<>());

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        auditLogServiceImpl.findStatistics();
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    @Test
    void findStatisticsSavedTest() {
        AuditLogStatisticsDao auditLogStatisticsDao = createAuditLogStatisticsOutput();

        when(statisticsRepository.findAll()).thenReturn(List.of(auditLogStatisticsDao));

        try {
            List<AuditLogStatisticsDao> auditLogStatisticsDaoList = auditLogServiceImpl.findStatisticsSaved();
            assertEquals(1, auditLogStatisticsDaoList.size());
            assertEquals(auditLogStatisticsDao, auditLogStatisticsDaoList.getFirst());
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    void indStatisticsSavedNullTest() {
        when(statisticsRepository.findAll()).thenReturn(new ArrayList<>());

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        auditLogServiceImpl.findStatisticsSaved();
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    @Test
    void refreshStatisticsSavedTest() {
        AuditLogStatisticsDao auditLogStatisticsDao = createAuditLogStatisticsOutput();

        when(auditLogRepository.statisticsAuditLog()).thenReturn(List.of(auditLogStatisticsDao));

        try {
            List<AuditLogStatisticsDao> auditLogStatisticsDaoList = auditLogServiceImpl.refreshStatisticsSaved();
            assertEquals(1, auditLogStatisticsDaoList.size());
            assertEquals(auditLogStatisticsDao, auditLogStatisticsDaoList.getFirst());
            verify(statisticsRepository, times(1)).deleteAll();
            verify(statisticsRepository, times(1)).saveAll(anyList());
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    void refreshStatisticsSavedErrorTest() {
        when(statisticsRepository.findAll()).thenReturn(new ArrayList<>());

        EntityNotFoundException entityNotFoundException = assertThrows(
                EntityNotFoundException.class,
                () -> {
                    try {
                        auditLogServiceImpl.findStatistics();
                    } catch (JDBCConnectionException e) {
                        fail();
                    }
                },
                StandardTexts.GENERAL_TROW_MESSAGE
        );
        assertTrue(entityNotFoundException.getMessage().contains(StandardTexts.DO_NOT_EXIST));
    }

    public AuditLogStatisticsDao createAuditLogStatisticsOutput() {
        AuditLogStatisticsDao auditLogStatisticsDao = new AuditLogStatisticsDao();
        auditLogStatisticsDao.setAction(AuditLogMock.ACTION);
        auditLogStatisticsDao.setTrace(AuditLogMock.TRACE.getFirst());
        auditLogStatisticsDao.setSum(AuditLogMock.SUM);
        auditLogStatisticsDao.setDate(AuditLogMock.SIMPLE_DATE);
        return auditLogStatisticsDao;

    }
}

