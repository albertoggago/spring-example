package org.example.alberto.serviceimpl;


import org.example.alberto.service.CustomerService;
import org.example.alberto.service.PersonalConfigurationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PersonalConfigurationServiceImplTest {

    @Autowired
    PersonalConfigurationService personalConfigurationService;
    @MockBean
    CustomerService customerService;

    @Test
    void testViewServiceImplValue() {

        assertTrue(personalConfigurationService.getTestMode());
    }



}