package org.example.alberto.controller;

import org.example.alberto.manager.StandardTexts;
import org.example.alberto.transformer.AuditLogMock;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.AuditLogDao;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.example.alberto.service.AuditLogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration
class AuditLogControllerTest {

    private final AuditLogDao auditLogDao = AuditLogMock.newAuditLogDao();
    @MockBean
    AuditLogService auditLogService;

    @Autowired
    private MockMvc mockMvc;

    AuditLogStatisticsDao auditLogStatisticsDao;
    List<AuditLogStatisticsDao> auditLogStatisticsDaoList;

    @BeforeEach
    public void init() {
        auditLogStatisticsDao = new AuditLogStatisticsDao();
        auditLogStatisticsDao.setAction(AuditLogMock.ACTION);
        auditLogStatisticsDao.setTrace(AuditLogMock.TRACE.getFirst());
        auditLogStatisticsDao.setSum(AuditLogMock.SUM);
        auditLogStatisticsDao.setDate(AuditLogMock.SIMPLE_DATE);

        auditLogStatisticsDaoList = List.of(auditLogStatisticsDao);
    }

    @Test
    void getAuditLogsTest() throws Exception {

        given(auditLogService.list(0, 1, "", "")).willReturn(Collections.singletonList(auditLogDao));

        String uri = "/auditLogs";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(AuditLogMock.ID_AUDIT_LOG)))
                .andExpect(jsonPath("$[0].action", is(AuditLogMock.ACTION)))
                .andExpect(jsonPath("$[0].createdDate", is(AuditLogMock.CREATED_DATE)));
    }

    @Test
    void getAuditLogsFailed() throws Exception {

        given(auditLogService.list(0, 1, "", "")).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, null));

        String uri = "/auditLogs";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void getAuditLogsFailedInvalidMongoFilter() throws Exception {

        given(auditLogService.list(0, 1, "", "")).willThrow(new InvalidMongoDbApiUsageException("Duplicated filters"));

        String uri = "/auditLogs";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.detail", is("Duplicated filters")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void getOneAuditLogTest() throws Exception {

        given(auditLogService.findDaoById(auditLogDao.getId())).willReturn(auditLogDao);

        String uri = "/auditLogs/" + AuditLogMock.ID_AUDIT_LOG;
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(AuditLogMock.ID_AUDIT_LOG)))
                .andExpect(jsonPath("$.action", is(AuditLogMock.ACTION)))
                .andExpect(jsonPath("$.createdDate", is(AuditLogMock.CREATED_DATE)));
    }

    @Test
    void getOneAuditLogFailedTest() throws Exception {

        given(auditLogService.findDaoById(auditLogDao.getId())).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, null));

        String uri = "/auditLogs/" + AuditLogMock.ID_AUDIT_LOG;
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void getStatisticsOkTest() throws Exception {

        given(auditLogService.findStatistics()).willReturn(Collections.singletonList(auditLogStatisticsDao));

        String uri = "/auditLogs/statistics";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].action", is(AuditLogMock.ACTION)))
                .andExpect(jsonPath("$[0].trace", is(AuditLogMock.TRACE.getFirst())))
                .andExpect(jsonPath("$[0].date", is(AuditLogMock.SIMPLE_DATE)));
    }

    @Test
    void getStatisticsFailedTest() throws Exception {

        given(auditLogService.findStatistics()).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, null));

        String uri = "/auditLogs/statistics";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void getStatisticsSavedOkTest() throws Exception {


        given(auditLogService.findStatisticsSaved()).willReturn(auditLogStatisticsDaoList);

        String uri = "/auditLogs/statisticsSaved";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].action", is(AuditLogMock.ACTION)))
                .andExpect(jsonPath("$[0].trace", is(AuditLogMock.TRACE.getFirst())))
                .andExpect(jsonPath("$[0].date", is(AuditLogMock.SIMPLE_DATE)));

    }

    @Test
    void getStatisticsSavedErrorTest() throws Exception {

        given(auditLogService.findStatisticsSaved()).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, null));

        String uri = "/auditLogs/statisticsSaved";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void refreshStatisticsSavedOkTest() throws Exception {

        given(auditLogService.refreshStatisticsSaved()).willReturn(auditLogStatisticsDaoList);

        String uri = "/auditLogs/refreshStatisticsSaved";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].action", is(AuditLogMock.ACTION)))
                .andExpect(jsonPath("$[0].trace", is(AuditLogMock.TRACE.getFirst())))
                .andExpect(jsonPath("$[0].date", is(AuditLogMock.SIMPLE_DATE)));

    }

    @Test
    void refreshStatisticsSavedErrorTest() throws Exception {

        given(auditLogService.refreshStatisticsSaved()).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, null));

        String uri = "/auditLogs/refreshStatisticsSaved";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

}
