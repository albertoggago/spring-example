package org.example.alberto.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.types.ObjectId;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.exception.BadDataException;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.service.CustomerService;
import org.example.alberto.transformer.CustomerMock;
import org.hibernate.JDBCException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration
class CustomerControllerTest {

    public static final String CONSTRAIN_ERROR = "CONSTRAIN ERROR";
    public static final String ERROR_SQL = "ERROR SQL";
    public static final String ID_AUDIT_LOG = "5ee94dd3850d2c08c122ffa6";
    @Autowired
    MockMvc mockMvc;

    private final CustomerDao customerDao = CustomerMock.newCustomerDao();

    @MockBean
    CustomerService customerService;

    private AuditLogEntity auditLogEntity;
    private ConstraintViolationException constraintViolationException;
    private JDBCException jdbcException;

    @BeforeEach
    void init(){
        auditLogEntity = new AuditLogEntity();
        auditLogEntity.setId(new ObjectId(ID_AUDIT_LOG));

        SQLException sqlException = new SQLException(CONSTRAIN_ERROR, "", new Throwable());
        SQLIntegrityConstraintViolationException sqlIntegrityConstraintViolationException = new SQLIntegrityConstraintViolationException("", sqlException);
        constraintViolationException = new ConstraintViolationException(ERROR_SQL, sqlIntegrityConstraintViolationException, "");
        jdbcException = new JDBCException("JDBC Exception", sqlException);
    }
    @Test
    void getCustomersTest() throws Exception {

        given(customerService.list(0, 1, "", "")).willReturn(Collections.singletonList(customerDao));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$[0].name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$[0].email", is(CustomerMock.EMAIL)));
    }

    @Test
    void getCustomersTestSortAndFilter() throws Exception {

        given(customerService.list(0, 1, "email DESC, id DESC", "id > 4"))
                .willReturn(Collections.singletonList(customerDao));

        String uri = "/customers" + "?" +
                     "page" + "=" + "0" + "&" +
                     "size" + "=" + "1" + "&" +
                     "sort" + "=" + "email DESC, id DESC" + "&" +
                     "filter" + "=" + "id > 4";

        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$[0].name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$[0].email", is(CustomerMock.EMAIL)));
    }
    @Test
    void getCustomersFailed() throws Exception {

        given(customerService.list(0, 1, "", ""))
             .willThrow(new EntityNotFoundException(StandardTexts.Error.ERROR_INFO_WRONG.toString(), ID_AUDIT_LOG));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.Error.ERROR_INFO_WRONG.toString())))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void getCustomersFailedBadInformation() throws Exception {

        given(customerService.list(0, 1, "", ""))
                .willThrow(new IllegalArgumentException(StandardTexts.Error.ERROR_BAD_REQUEST.toString()));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.Error.ERROR_BAD_REQUEST.toString())))
                .andExpect(jsonPath("$.instance", is(uri)));
    }
    @Test
    void getOneCustomerTest() throws Exception {

        given(customerService.findCustomerByAlias(CustomerMock.ALIAS)).willReturn(customerDao);

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$.name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$.email", is(CustomerMock.EMAIL)));
    }

    @Test
    void getNothingCustomerTest() throws Exception {

        given(customerService.findCustomerByAlias(StandardTexts.NOTHING)).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, ID_AUDIT_LOG));

        String uri = "/customers/" + StandardTexts.NOTHING;
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void postCustomerTest() throws Exception {

        given(customerService.createCustomer(any())).willReturn(customerDao);

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.CREATED.value()))
                .andExpect(jsonPath("$.alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$.name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$.email", is(CustomerMock.EMAIL)));
    }

    @Test
    void postCustomerDuplicate() throws Exception {

        given(customerService.createCustomer(any())).willThrow(constraintViolationException);

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("$.title", is(StandardTexts.Error.ERROR_INFO_WRONG.toString())))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.detail", is("ERROR SQL; Cause:CONSTRAIN ERROR")));
    }

    @Test
    void postCustomerBadResponse() throws Exception {

        given(customerService.createCustomer(any())).willThrow(new BadDataException(StandardTexts.SOME_ELEMENT_IS_NOT_VALID, auditLogEntity));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.SOME_ELEMENT_IS_NOT_VALID)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void putCustomerTest() throws Exception {

        given(customerService.updatedCustomer(any())).willReturn(customerDao);

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(customerDao))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$.name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$.email", is(CustomerMock.EMAIL)));
    }

    @Test
    void putCustomerTestErrorNotFound() throws Exception {

        given(customerService.updatedCustomer(any())).willThrow(new EntityNotFoundException(StandardTexts.DO_NOT_EXIST, ID_AUDIT_LOG));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void putCustomerTestErrorNotValid() throws Exception {

        given(customerService.updatedCustomer(any())).willThrow(new BadDataException(StandardTexts.SOME_ELEMENT_IS_NOT_VALID, auditLogEntity));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.SOME_ELEMENT_IS_NOT_VALID)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void putCustomerTestErrorGeneralError() throws Exception {

        given(customerService.updatedCustomer(any())).willThrow(constraintViolationException);

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("$.title", is(StandardTexts.Error.ERROR_INFO_WRONG.toString())))
                .andExpect(jsonPath("$.status", is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath("$.detail", is("ERROR SQL; Cause:CONSTRAIN ERROR")));
    }

    @Test
    void postCustomerTestError() throws Exception {

        given(customerService.createCustomer(any())).willThrow(new BadDataException(StandardTexts.DO_NOT_EXIST, auditLogEntity));

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.detail", is(StandardTexts.DO_NOT_EXIST)))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    void deleteAllCustomerTest() throws Exception {

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.ACCEPTED.value()));
    }

    @Test
    void deleteAllCustomerTestNotFound() throws Exception {

        doThrow(new EntityNotFoundException(StandardTexts.SOME_ELEMENT_IS_NOT_VALID, ID_AUDIT_LOG)).when(customerService).deleteAllCustomer();

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    void deleteAllCustomerTestGeneralError() throws Exception {

        doThrow(constraintViolationException).when(customerService).deleteAllCustomer();

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    void deleteOneCustomerTest() throws Exception {

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.ACCEPTED.value()));
    }

    @Test
    void deleteOneCustomerTestNotFound() throws Exception {

        doThrow(new EntityNotFoundException(StandardTexts.SOME_ELEMENT_IS_NOT_VALID, ID_AUDIT_LOG)).when(customerService).deleteOneCustomer(any());

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    void deleteOneCustomerTestGeneralError() throws Exception {

        doThrow(constraintViolationException).when(customerService).deleteOneCustomer(any());

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testJDBCException() throws Exception {
        doThrow(jdbcException).when(customerService).deleteOneCustomer(any());

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }


}
