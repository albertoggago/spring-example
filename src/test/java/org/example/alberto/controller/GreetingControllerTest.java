package org.example.alberto.controller;

import org.example.alberto.model.GreetingDao;
import org.example.alberto.service.GreetingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration
class GreetingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    GreetingService greetingService;

    @Test
    void getGreeting() throws Exception {
        String uri="/greeting";
        GreetingDao greetingDao = new GreetingDao().id(1L).content("Hello, World!");

        given(greetingService.getGreeting(any())).willReturn(greetingDao);

        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.content", is("Hello, World!")));
    }

    @Test
    void getGreetingWithName() throws Exception {
        String uri="/greeting?name=Bertie";
        GreetingDao greetingDao = new GreetingDao().id(2L).content("Hello, Bertie!");

        given(greetingService.getGreeting(any())).willReturn(greetingDao);

        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.content", is("Hello, Bertie!")));
    }

}
