package org.example.alberto.dao;

import org.example.alberto.model.CustomerDao;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerDaoTest {


    @Test
    void toStringTest() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setId(1L);

        String timeStampText = "2020-06-08 17:45:26.123";
        customerDao.setCreationDate(timeStampText);
        customerDao.setUpdatedDate(timeStampText);

        assertEquals("""
                class CustomerDao {
                    id: 1
                    alias: alias
                    name: name
                    email: example@domain.com
                    creationDate: 2020-06-08 17:45:26.123
                    updatedDate: 2020-06-08 17:45:26.123
                }""", customerDao.toString());
    }
}
