package org.example.alberto.dao;

import org.example.alberto.model.AuditLogDao;
import org.example.alberto.transformer.AuditLogMock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


class AuditLogDaoTest {

    @Test
    void testToText() {
        AuditLogDao auditLogDao = AuditLogMock.newAuditLogDao();

        String toStringTest = auditLogDao.toString();

        assertTrue(toStringTest.contains("id: " + AuditLogMock.ID_AUDIT_LOG), toStringTest);
        assertTrue(toStringTest.contains("action: " + AuditLogMock.ACTION), toStringTest);
        assertTrue(toStringTest.contains("trace: " + AuditLogMock.TRACE), toStringTest);
        assertTrue(toStringTest.contains("parameters: " + AuditLogMock.PROPERTIES), toStringTest);
        assertTrue(toStringTest.contains("parameters: " + AuditLogMock.PROPERTIES), toStringTest);
        assertTrue(toStringTest.contains("inputObject: " + AuditLogMock.INPUT_OBJECT), toStringTest);
        assertTrue(toStringTest.contains("outputObject: " + AuditLogMock.OUTPUT_OBJECT), toStringTest);
        assertTrue(toStringTest.contains("error: " + AuditLogMock.ERROR), toStringTest);
        assertTrue(toStringTest.contains("traceError: " + AuditLogMock.TRACE_ERROR), toStringTest);

        assertTrue(toStringTest.contains("createdDate: " + AuditLogMock.CREATED_DATE), toStringTest);
        assertTrue(toStringTest.contains("updatedDate: " + AuditLogMock.UPDATED_DATE), toStringTest);
        assertTrue(toStringTest.contains("endDate: " + AuditLogMock.END_DATE), toStringTest);

    }
}