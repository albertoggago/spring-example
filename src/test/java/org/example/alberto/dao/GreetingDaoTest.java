package org.example.alberto.dao;

import org.example.alberto.model.GreetingDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class GreetingDaoTest {
    private static final Long ID = 11L;
    private static final String CONTENT = "Me";
    private GreetingDao greetingDao;

    @BeforeEach
    void init() {
        greetingDao = new GreetingDao().id(ID).content(CONTENT);
    }
    @Test
    void greetingDaoCreation (){


        assertEquals(ID, greetingDao.getId());
        assertEquals(CONTENT, greetingDao.getContent());
    }

    @Test
    void greetingDaoToString() {
        String toString = greetingDao.toString();

        assertEquals("""
    class GreetingDao {
        id: 11
        content: Me
    }""",toString);
    }


}
