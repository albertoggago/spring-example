package org.example.alberto.transformer;

import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.model.CustomerDao;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTransformerTest {


    @Test
    void testCustomerDaoTransformer() {
        CustomerEntity customerEntity = CustomerMock.newCustomerEntity();

        CustomerDao customerDaoNew = CustomerTransformer.transformToDao(customerEntity, null);

        assertEquals(customerEntity.getId(), customerDaoNew.getId());
        assertEquals(customerEntity.getAlias(), customerDaoNew.getAlias());
        assertEquals(customerEntity.getName(), customerDaoNew.getName());
        assertEquals(customerEntity.getEmail(), customerDaoNew.getEmail());
        assertEquals(CustomerMock.TIME_STAMP_TEXT, customerDaoNew.getCreationDate());
        assertEquals(CustomerMock.TIME_STAMP_TEXT, customerDaoNew.getUpdatedDate());
    }

    @Test
    void testCustomerDaoCompress() {
        CustomerEntity customerEntity = CustomerMock.newCustomerEntity();

        CustomerDao customerDaoNew = CustomerTransformer.transformToDao(customerEntity);

        assertEquals(customerEntity.getId(), customerDaoNew.getId());
        assertEquals(customerEntity.getAlias(), customerDaoNew.getAlias());
        assertEquals(customerEntity.getName(), customerDaoNew.getName());
        assertEquals(customerEntity.getEmail(), customerDaoNew.getEmail());
        assertEquals(CustomerMock.TIME_STAMP_TEXT, customerDaoNew.getCreationDate());
        assertEquals(CustomerMock.TIME_STAMP_TEXT, customerDaoNew.getUpdatedDate());
    }

    @Test
    void testCustomerDaoTransformerNull() {
        CustomerDao customerDaoNew = CustomerTransformer.transformToDao(null, new CustomerDao());

        assertNull(customerDaoNew.getId());
        assertNull(customerDaoNew.getAlias());
    }

    @Test
    void testCustomerDaoEntityDouble() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        CustomerEntity customerEntityNew = new CustomerEntity();

        CustomerTransformer.transformToEntity(customerDao, customerEntityNew);

        CustomerDao customerDaoNew = CustomerTransformer.transformToDao(customerEntityNew, null);

        assertEquals(customerDao, customerDaoNew);
    }

    @Test
    void testCustomerEntityTransformer() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();

        CustomerEntity customerEntityNew = new CustomerEntity();
        CustomerTransformer.transformToEntity(customerDao, customerEntityNew);

        assertEquals(customerDao.getId(), customerEntityNew.getId());
        assertEquals(customerDao.getName(), customerEntityNew.getName());
        assertEquals(customerDao.getAlias(), customerEntityNew.getAlias());
        assertEquals(customerDao.getEmail(), customerEntityNew.getEmail());
        assertEquals(new Timestamp(CustomerMock.TIME_STAMP_NUM), customerEntityNew.getCreationDate());
        assertEquals(new Timestamp(CustomerMock.TIME_STAMP_NUM), customerEntityNew.getUpdatedDate());
    }

    @Test
    void testCustomerEntityTransformerCompress() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();

        CustomerEntity customerEntityNew = CustomerTransformer.transformToEntity(customerDao);

        assertEquals(customerDao.getId(), customerEntityNew.getId());
        assertEquals(customerDao.getName(), customerEntityNew.getName());
        assertEquals(customerDao.getAlias(), customerEntityNew.getAlias());
        assertEquals(customerDao.getEmail(), customerEntityNew.getEmail());
        assertEquals(new Timestamp(CustomerMock.TIME_STAMP_NUM), customerEntityNew.getCreationDate());
        assertEquals(new Timestamp(CustomerMock.TIME_STAMP_NUM), customerEntityNew.getUpdatedDate());
    }

    @Test
    void testCustomerEntityTransformerNull() {

        CustomerEntity customerEntityNew = CustomerTransformer.transformToEntity(null, null);

        assertNull(customerEntityNew.getId());
        assertNull(customerEntityNew.getName());
    }

    @Test
    void testCustomerEntityDateError() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setCreationDate("2020-13-13");
        customerDao.setUpdatedDate("2020-13-13A");

        CustomerEntity customerEntityNew = new CustomerEntity();
        CustomerTransformer.transformToEntity(customerDao, customerEntityNew);

        assertNull(customerEntityNew.getCreationDate());
        assertNull(customerEntityNew.getUpdatedDate());
    }

    @Test
    void testCustomerEntityDaoDouble() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();

        CustomerEntity customerEntityNew = new CustomerEntity();
        CustomerTransformer.transformToEntity(customerDao, customerEntityNew);

        CustomerDao customerDaoNew = new CustomerDao();
        CustomerTransformer.transformToDao(customerEntityNew, customerDaoNew);

        assertEquals(customerDao, customerDaoNew);
    }

    @Test
    void testCustomerDaoEntityNullDates() {
        CustomerEntity customerEntity = CustomerMock.newCustomerEntity();
        customerEntity.setCreationDate(null);
        customerEntity.setUpdatedDate(null);

        CustomerDao customerDao = CustomerTransformer.transformToDao(customerEntity);

        assertNull(customerDao.getCreationDate());
        assertNull(customerDao.getUpdatedDate());
    }

}
