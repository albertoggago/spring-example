package org.example.alberto.transformer;

import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.model.CustomerDao;

import java.sql.Timestamp;

public class CustomerMock {
    public static final String NAME = "name";
    public static final String ALIAS = "alias";
    public static final long TIME_STAMP_NUM = 1591638326123L; // 8/06/2020, 18:45:26.045 my timezone.
    public static final String EMAIL = "example@domain.com";
    public static final String TIME_STAMP_TEXT = "2020-06-08 17:45:26.123";


    public static CustomerEntity newCustomerEntity() {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(1L);
        customerEntity.setName(NAME);
        customerEntity.setAlias(ALIAS);
        customerEntity.setEmail(EMAIL);
        customerEntity.setCreationDate(new Timestamp(TIME_STAMP_NUM));
        customerEntity.setUpdatedDate(new Timestamp(TIME_STAMP_NUM));
        return customerEntity;
    }

    public static CustomerDao newCustomerDao(){
        CustomerDao customerDao = new CustomerDao();
        customerDao.setId(1L);
        customerDao.setCreationDate(TIME_STAMP_TEXT);
        customerDao.setUpdatedDate(TIME_STAMP_TEXT);
        customerDao.setName(NAME);
        customerDao.setAlias(ALIAS);
        customerDao.setEmail(EMAIL);
        return customerDao;
    }

    public static CustomerDao createCustomerDaoInsert() {
        CustomerDao customerDao = newCustomerDao();
        customerDao.setId(null);
        customerDao.setCreationDate(null);
        customerDao.setUpdatedDate(null);
        return customerDao;
    }
}
