package org.example.alberto.transformer;

import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.model.AuditLogDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

class AuditLogTransformerTest {

    AuditLogDao auditLogDao;
    AuditLogEntity auditLogEntity;

    @BeforeEach
    void setUp() {
        auditLogDao = AuditLogMock.newAuditLogDao();

        auditLogEntity = AuditLogMock.newAuditLogEntity();
    }

    @Test
    void transformToDaoTest() {
        AuditLogDao auditLogDaoNew = new AuditLogDao();

        AuditLogTransformer.transformToDao(auditLogEntity, auditLogDaoNew);

        assertEquals(auditLogDao, auditLogDaoNew);
    }

    @Test
    void transformToDaoNullTest() {
        AuditLogDao auditLogDaoNew = new AuditLogDao();

        AuditLogTransformer.transformToDao(null, auditLogDaoNew);

        assertNull(auditLogDaoNew.getId());
        assertNull(auditLogDaoNew.getAction());
    }


    @Test
    void transformToEntityTest() {
        AuditLogEntity auditLogEntityNew = new AuditLogEntity();

        AuditLogTransformer.transformToEntity(auditLogDao, auditLogEntityNew);

        assertEquals(auditLogEntity, auditLogEntityNew);
    }

    @Test
    void transformToEntityNullTest() {
        AuditLogEntity auditLogEntityNew = new AuditLogEntity();

        AuditLogTransformer.transformToEntity(null, auditLogEntityNew);

        assertNull(auditLogEntityNew.getId());
        assertNull(auditLogEntityNew.getAction());

    }

    @Test
    void auditLogEntityDoubleTest() {
        AuditLogDao auditLogDaoNew = new AuditLogDao();
        AuditLogTransformer.transformToDao(auditLogEntity, auditLogDaoNew);

        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        AuditLogTransformer.transformToEntity(auditLogDaoNew, auditLogEntityNew);

        assertEquals(auditLogEntity, auditLogEntityNew);
    }

    @Test
    void auditLogDaoDoubleTest() {
        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        AuditLogTransformer.transformToEntity(auditLogDao, auditLogEntityNew);

        AuditLogDao auditLogDaoNew = new AuditLogDao();
        AuditLogTransformer.transformToDao(auditLogEntityNew, auditLogDaoNew);

        assertEquals(auditLogDao, auditLogDaoNew);
    }

    @Test
    void auditLogTransformToEntityNotNulls() {
        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        auditLogEntityNew.setAction("NOT NULL");
        auditLogDao.setAction(null);
        AuditLogTransformer.transformToEntityNotNulls(auditLogDao, auditLogEntityNew);

        assertNotNull(auditLogEntityNew.getAction());
    }

    @Test
    void creationDateNullTest() {
        AuditLogDao auditLogDaoNew = new AuditLogDao();
        auditLogEntity.setCreatedDate(null);

        AuditLogTransformer.transformToDao(auditLogEntity, auditLogDaoNew);

        assertNull(auditLogDaoNew.getCreatedDate());
    }

    @Test
    void creationDateErrorTest() {
        AuditLogEntity auditLogEntityNew = new AuditLogEntity();
        auditLogDao.setCreatedDate(null);
        auditLogDao.setUpdatedDate("2020-02-31");
        auditLogDao.setEndDate("2020-xx-30");

        AuditLogTransformer.transformToEntity(auditLogDao, auditLogEntityNew);

        assertNull(auditLogEntityNew.getUpdatedDate());
        assertNull(auditLogEntityNew.getCreatedDate());
        assertNull(auditLogEntityNew.getEndDate());
    }

    @Test
    void creationObject() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor<AuditLogTransformer> constructor = AuditLogTransformer.class.getDeclaredConstructor();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        constructor.setAccessible(true);

        AuditLogTransformer result = constructor.newInstance();

        assertNotNull(result);
    }


}