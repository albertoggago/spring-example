package org.example.alberto.transformer;

import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.AuditLogDao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class AuditLogMock {

    public static final String ID_AUDIT_LOG = "5ee94dd3850d2c08c122ffa6";
    public static final String ACTION = "action";
    public static final String CREATED_DATE = "2020-01-01 12:12:11.111";
    public static final String UPDATED_DATE = "2020-01-02 12:12:11.111";
    public static final String END_DATE = "2020-01-03 12:12:11.111";

    public static final String SIMPLE_DATE = "2020-01-04 12:12:11.111";
    public static final List<String> TRACE = Arrays.asList("TRACE1", "TRACE2");
    public static final Map<String, String> PROPERTIES =
            Map.of("param1", "value1", "param2", "value2");

    public static final String INPUT_OBJECT = "InputObject";
    public static final String OUTPUT_OBJECT = "OutputObject";
    public static final String ERROR = "ERROR";
    public static final List<String> TRACE_ERROR = Arrays.asList("TRACE_ERROR1", "TRACE_ERROR2");

    public static final Long SUM = 10L;

    public static AuditLogDao newAuditLogDao() {
        AuditLogDao auditLogDao = new AuditLogDao();
        auditLogDao.setId(ID_AUDIT_LOG);
        auditLogDao.setAction(ACTION);
        auditLogDao.setTrace(TRACE);
        auditLogDao.setParameters(PROPERTIES);
        auditLogDao.setInputObject(INPUT_OBJECT);
        auditLogDao.setOutputObject(OUTPUT_OBJECT);
        auditLogDao.setError(ERROR);
        auditLogDao.setTraceError(TRACE_ERROR);

        auditLogDao.setCreatedDate(CREATED_DATE);
        auditLogDao.setUpdatedDate(UPDATED_DATE);
        auditLogDao.setEndDate(END_DATE);

        return auditLogDao;
    }

    public static AuditLogEntity newAuditLogEntity() {
        AuditLogEntity auditLogEntity = new AuditLogEntity();
        auditLogEntity.setId(new ObjectId(ID_AUDIT_LOG));
        auditLogEntity.setAction(ACTION);
        auditLogEntity.setTrace(TRACE);
        auditLogEntity.setParameters(PROPERTIES);
        auditLogEntity.setInputObject(INPUT_OBJECT);
        auditLogEntity.setOutputObject(OUTPUT_OBJECT);
        auditLogEntity.setError(ERROR);
        auditLogEntity.setTraceError(TRACE_ERROR);

        DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
        dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));

        try {
            auditLogEntity.setCreatedDate(new Timestamp(dateFormat.parse(CREATED_DATE).getTime()));
            auditLogEntity.setUpdatedDate(new Timestamp(dateFormat.parse(UPDATED_DATE).getTime()));
            auditLogEntity.setEndDate(new Timestamp(dateFormat.parse(END_DATE).getTime()));
        } catch (ParseException e) {
            log.error("Error in Mock" ,e);
        }

        return auditLogEntity;
    }

}
