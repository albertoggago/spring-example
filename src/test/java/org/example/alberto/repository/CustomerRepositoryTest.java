package org.example.alberto.repository;

import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


@SpringBootTest
@EnableAutoConfiguration
class CustomerRepositoryTest {

    private static final String NEW_NAME = "new name";
    private static final String NEW_EMAIL = "newEmail@gmail.com";
    private static final String NEW_ALIAS = "newalias";

    @Autowired
    private CustomerRepository customerRepository;

    private CustomerEntity customerEntity;


    @BeforeEach
    void setUp() {
        customerEntity = CustomerMock.newCustomerEntity();
        customerRepository.save(customerEntity);
    }

    @AfterEach
    void clean(){
        customerRepository.deleteAll();
    }

    @Test
    void findAllTest() {

        Page<CustomerEntity> customers = customerRepository.findAll(PageRequest.of(0,100));

        Assertions.assertEquals(1, customers.getContent().size());
    }

    @Test
    void findByAliasTest() {
        CustomerEntity customer = customerRepository.findByAlias(CustomerMock.ALIAS);

        Assertions.assertNotNull(customer.getId());
        Assertions.assertEquals(CustomerMock.ALIAS, customer.getAlias());
        Assertions.assertEquals(CustomerMock.NAME, customer.getName());
        Assertions.assertEquals(CustomerMock.EMAIL, customer.getEmail());
    }

    @Test
    void insert() {
        Assertions.assertNotNull(customerEntity.getId());
        Assertions.assertEquals(CustomerMock.ALIAS, customerEntity.getAlias());
        Assertions.assertEquals(CustomerMock.NAME, customerEntity.getName());
        Assertions.assertEquals(CustomerMock.EMAIL, customerEntity.getEmail());
        Assertions.assertNotNull(customerEntity.getCreationDate());
        Assertions.assertNotNull(customerEntity.getUpdatedDate());
    }

    @Test
    void update() {
        CustomerEntity customerEntityNew = customerRepository.findByAlias(CustomerMock.ALIAS);

        customerEntityNew.setAlias(NEW_ALIAS);
        customerEntityNew.setEmail(NEW_EMAIL);
        customerEntityNew.setName(NEW_NAME);

        customerRepository.save(customerEntityNew);

        CustomerEntity customerEntityNewFound = customerRepository.findByAlias(customerEntityNew.getAlias());

        Assertions.assertNotNull(customerEntityNewFound.getId());
        Assertions.assertEquals(NEW_ALIAS, customerEntityNewFound.getAlias());
        Assertions.assertEquals(NEW_NAME, customerEntityNewFound.getName());

        Assertions.assertEquals(NEW_EMAIL, customerEntityNewFound.getEmail());
        Assertions.assertNotNull(customerEntityNewFound.getUpdatedDate());
    }
}
