package org.example.alberto.entity;

import org.bson.types.ObjectId;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AuditLogEntityTest {

    @Test
    void testCreation() {

        AuditLogEntity auditLogEntity = new AuditLogEntity();
        auditLogEntity.setId(new ObjectId("5ee94dd3850d2c08c122ffa6"));
        auditLogEntity.setAction("action");

        auditLogEntity.setTrace(new ArrayList<>(Arrays.asList("ONE", "SECOND")));

        Map<String, String> myMap = new HashMap<>();
        myMap.put("one", "1");
        myMap.put("two", "2");
        auditLogEntity.setParameters(myMap);

        auditLogEntity.setInputObject(CustomerMock.newCustomerEntity());
        auditLogEntity.setOutputObject(CustomerMock.newCustomerDao());


        auditLogEntity.setError("error");
        auditLogEntity.setTraceError(new ArrayList<>(Collections.singleton("traceError")));

        auditLogEntity.setCreatedDate(new Date());
        auditLogEntity.setUpdatedDate(new Date());
        auditLogEntity.setEndDate(new Date());


        assertEquals("5ee94dd3850d2c08c122ffa6", auditLogEntity.getId().toHexString());
        assertEquals("action", auditLogEntity.getAction());

        assertEquals("ONE", auditLogEntity.getTrace().get(0));
        assertEquals("SECOND", auditLogEntity.getTrace().get(1));

        assertEquals("1", auditLogEntity.getParameters().get("one"));
        assertEquals("2", auditLogEntity.getParameters().get("two"));

        assertEquals("error", auditLogEntity.getError());
        assertEquals("traceError", auditLogEntity.getTraceError().getFirst());

        assertNotNull(auditLogEntity.getCreatedDate());
        assertNotNull(auditLogEntity.getUpdatedDate());
        assertNotNull(auditLogEntity.getEndDate());
    }

    @Test
    void testAddTrace() {
        AuditLogEntity auditLogEntity = new AuditLogEntity();

        auditLogEntity.addTrace("ONE");
        auditLogEntity.addTrace("SECOND");

        assertEquals("ONE", auditLogEntity.getTrace().get(0));
        assertEquals("SECOND", auditLogEntity.getTrace().get(1));
    }
}
