package org.example.alberto.entity;

import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CustomerEntityTest {

    CustomerEntity customerEntity;

    @BeforeEach
    void setup() {
        customerEntity = CustomerMock.newCustomerEntity();
    }

    @Test
    void customerEntityCreationTest() {
        Assertions.assertEquals(CustomerMock.ALIAS, customerEntity.getAlias());
        Assertions.assertEquals(CustomerMock.NAME, customerEntity.getName());
        Assertions.assertEquals(CustomerMock.EMAIL, customerEntity.getEmail());
    }
}
