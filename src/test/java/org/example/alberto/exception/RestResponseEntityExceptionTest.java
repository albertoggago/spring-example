package org.example.alberto.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RestResponseEntityExceptionTest {

    @Test
    void getFinalCause() {
        Throwable throwable = new Throwable("text");
        String finalCause = RestResponseEntityExceptionHandler.getFinalCause(throwable);
        assertEquals("text", finalCause);
    }
}
