package org.example.alberto.manager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.data.domain.Sort;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class StandardManagerTest {
    private final String standardField = "creationDate";
    private final List<String> fields = Arrays.asList("name", "alias", "creationDate", "updatedDate");

    private static final Map<String, Map<String,String>> COMP_TOKEN = new HashMap<>();
    static {
        COMP_TOKEN.put("id", Map.of("=", "accEqual"));
        COMP_TOKEN.put("action", Map.of("is", "accIs","is_not", "accIsNot"));
    }

    @Test
    void createSortNull() {

        List<Sort.Order> result = StandardManager
                .getSortOrder("", fields, standardField);

        assertEquals(1, result.size());
        assertEquals(Collections.singletonList(new Sort.Order(Sort.Direction.DESC, "creationDate")), result);
    }

    @Test
    void createSortOk() {

        List<Sort.Order> result = StandardManager
                .getSortOrder("alias asc, creationDate desc,updatedDate asc", fields, standardField);


        assertEquals(3, result.size());
        assertEquals(Arrays.asList(new Sort.Order(Sort.Direction.ASC, "alias"),
                new Sort.Order(Sort.Direction.DESC, "creationDate"),
                new Sort.Order(Sort.Direction.ASC, "updatedDate")
        ), result);
    }

    @Test
    void createSortElementNotDef() {

        List<Sort.Order> result = StandardManager
                .getSortOrder("alias, creationDate desc,updatedDate asc", fields, standardField);


        assertEquals(3, result.size());
        assertEquals(Arrays.asList(new Sort.Order(Sort.Direction.ASC, "alias"),
                new Sort.Order(Sort.Direction.DESC, "creationDate"),
                new Sort.Order(Sort.Direction.ASC, "updatedDate")
        ), result);
    }

    @Test
    void createSortElementTooComponents() {

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> StandardManager.getSortOrder("alias asc not_valid", fields, standardField));
        assertEquals("Sort too elements: alias asc not_valid", result.getMessage());

    }

    @Test
    void createSortErrorElementDoesNotExist() {

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> StandardManager.getSortOrder("aliases asc", fields, standardField));
        assertEquals("Sort element does not exist: aliases", result.getMessage());
    }

    @Test
    void createSortErrorTypeSortDoesNotExist() {

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> StandardManager.getSortOrder("aliases hello", fields, standardField));
        assertEquals("Invalid value 'hello' for orders given; Has to be either 'desc' or 'asc' (case insensitive)", result.getMessage());
    }

    static Stream<Arguments> generateData() {
        return Stream.of(
                Arguments.of( "id = 1234", List.of(new SearchElement("accEqual", "id", "1234"))),
                Arguments.of( "action is null", List.of(new SearchElement("accIs", "action", "null"))),
                Arguments.of( "action is_not null", List.of(new SearchElement("accIsNot", "action", "null")))
        );
    }

    @ParameterizedTest
    @MethodSource("generateData")
    void generateSearchExampleToListOK(String input, List<SearchElement> output){
        List<SearchElement> result = StandardManager
                .generateSearchExampleToList(input, COMP_TOKEN);

        assertEquals(1, result.size());
        assertEquals(output.getFirst().element(), result.getFirst().element());
        assertEquals(output.getFirst().type(), result.getFirst().type());
        assertEquals(output.getFirst().value(), result.getFirst().value());
    }

    @ParameterizedTest
    @CsvSource({"no filter,'Filter Parameter Issue, malformed: no filter'",
                "alias like al,'Filter Parameter Issue, row does not exist: alias'",
                "action like al,'Filter Parameter Issue, incompatible comparator: action and comparator: like'",
                "action is al,'Error is with no null, variable: action and comparator: is'",
                "action is_not al,'Error is with no null, variable: action and comparator: is_not'"
    })
    void generateSearchExampleToListOK(String input, String output){

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> StandardManager.generateSearchExampleToList(input, COMP_TOKEN));
        assertEquals(output, result.getMessage());
    }
}
