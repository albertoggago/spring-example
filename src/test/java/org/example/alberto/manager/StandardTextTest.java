package org.example.alberto.manager;

import org.example.alberto.model.CustomerDao;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;


class StandardTextTest {

    @Test
    void getNullPropertyNamesTest() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setName(null);

        String[] nullComponents = StandardTexts.getNullPropertyNames(customerDao);

        assertNotEquals(0, nullComponents.length);
    }
}
