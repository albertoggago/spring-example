package org.example.alberto.manager;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.data.mongodb.core.query.Query;

import static org.junit.jupiter.api.Assertions.*;

class AuditLogManagerTest {

    @ParameterizedTest
    @CsvSource({"id = aaa, 'Query: { \"id\" : \"aaa\"}, Fields: {}, Sort: {}'",
                "action like al, 'Query: { \"action\" : { \"$regularExpression\" : { \"pattern\" : \".*al.*\", \"options\" : \"i\"}}}, Fields: {}, Sort: {}'",
                "error is null,'Query: { \"error\" : null}, Fields: {}, Sort: {}'",
                "action not al,'Query: { \"action\" : { \"$ne\" : \"al\"}}, Fields: {}, Sort: {}'",
                "error is_not null,'Query: { \"error\" : { \"$ne\" : null}}, Fields: {}, Sort: {}'",
                "createdDate day 2013-11-15,'Query: { \"$and\" : [{ \"createdDate\" : { \"$gt\" : { \"$date\" : \"2013-11-15T00:00:00Z\"}}}, { \"createdDate\" : { \"$lt\" : { \"$date\" : \"2013-11-16T00:00:00Z\"}}}]}, Fields: {}, Sort: {}'"})
    void generateSearchExampleToListOK(String input, String output) {

        Query result = AuditLogManager
                .generateSearchExample(new Query(), input);

        assertEquals(output, result.toString());
    }



    @ParameterizedTest
    @CsvSource(value = {"id like Juan#Filter Parameter Issue, incompatible comparator: id and comparator: like",
                        "createdDate day 2013-11#Filter Date Issue, component: createdDate, date:2013-11"
            }
            , delimiter= '#')
    void generateSearchExampleComparatorError(String input, String output){

        Throwable result = assertThrows(Throwable.class, () ->
                AuditLogManager.generateSearchExample(new Query(), input));

        assertInstanceOf(IllegalArgumentException.class,result);
        assertEquals(output, result.getMessage());
    }
}
