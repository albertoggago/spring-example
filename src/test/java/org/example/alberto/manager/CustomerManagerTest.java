package org.example.alberto.manager;

import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.data.jpa.domain.Specification;


import static org.junit.jupiter.api.Assertions.*;

class CustomerManagerTest {

    @Test
    void okInsertValidation() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setId(null);
        customerDao.creationDate(null);
        customerDao.updatedDate(null);

        CustomerManager.verifyInsert(customerDao);

        assertTrue(true);
    }
    @Test
    void errorInsertWithNull() {

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> CustomerManager.verifyInsert(null));

        assertEquals(StandardTexts.DO_NOT_EXIST, result.getMessage());
    }
    @Test
    void errorInsertWithID() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.creationDate(null);
        customerDao.updatedDate(null);

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> CustomerManager.verifyInsert(customerDao));

        assertEquals("id " + StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER,
                result.getMessage());
    }

    @Test
    void errorInsertWithCreationDate() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setId(null);
        customerDao.updatedDate(null);

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> CustomerManager.verifyInsert(customerDao));

        assertEquals("creationDate " + StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER,
                result.getMessage());
    }

    @Test
    void errorInsertWithUpdatedDate() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setId(null);
        customerDao.creationDate(null);

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> CustomerManager.verifyInsert(customerDao));

        assertEquals("updatedDate " + StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER,
                result.getMessage());
    }

    @Test
    void okUpdateValidation() {
        CustomerDao customerDao = CustomerMock.newCustomerDao();

        CustomerManager.verifyUpdate(customerDao);

        assertTrue(true);
    }

    @Test
    void errorUpdateWithNull() {
        IllegalArgumentException result = assertThrows(IllegalArgumentException.class,
                () -> CustomerManager.verifyUpdate(null));

        assertEquals(StandardTexts.CUSTOMER + " " + StandardTexts.DO_NOT_EXIST, result.getMessage());
    }

    @Test
    void updateDatesInsertTest(){
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setCreationDate(null);

        assertNull(customerDao.getCreationDate());
        CustomerManager.updateDatesInsert(customerDao);

        assertNotNull(customerDao.getCreationDate());
    }

    @Test
    void updateDatesUpdatedTest(){
        CustomerDao customerDao = CustomerMock.newCustomerDao();
        customerDao.setUpdatedDate(null);

        assertNull(customerDao.getUpdatedDate());
        CustomerManager.updateDatesUpdate(customerDao);

        assertNotNull(customerDao.getUpdatedDate());
    }

    @ParameterizedTest
    @ValueSource(strings = {"name = alberto", "id = 123", "id > 10", "id < 1110",
                            "name like mat", "creationDate day 2022-10-11"})
    void generateSearchExampleToListOK(String input) {

        Specification<CustomerEntity> result = CustomerManager
                .generateSearchExample(input);

        assertNotNull(result);
    }



    @ParameterizedTest
    @CsvSource(value = {"name LAKE Juan#Filter Parameter Issue, incompatible comparator: name and comparator: lake",
            "name = Juan Juan#Filter Parameter Issue, malformed: name = Juan Juan",
            "nome like Juan#Filter Parameter Issue, row does not exist: nome",
            "id like 7777#Filter Parameter Issue, incompatible comparator: id and comparator: like",
            "id = Juan#For input string: \"Juan\"",
            "creationDate like 2022#Filter Parameter Issue, incompatible comparator: creationDate and comparator: like",
            "updatedDate like 2022#Filter Parameter Issue, incompatible comparator: updatedDate and comparator: like",
            "creationDate = 2022#Filter Parameter Issue, incompatible comparator: creationDate and comparator: =",
            "updatedDate = 2022#Filter Parameter Issue, incompatible comparator: updatedDate and comparator: =",
            "creationDate day 2022#Filter Date Issue, component: creationDate, date:2022",
            "updatedDate day 2022#Filter Date Issue, component: updatedDate, date:2022"}
            , delimiter= '#')
    void generateSearchExampleComparatorError(String input, String output){

        IllegalArgumentException result = assertThrows(IllegalArgumentException.class, () ->
                CustomerManager.generateSearchExample(input));

        assertEquals(output, result.getMessage());
    }
}
