package org.example.alberto.mongdb;

import org.example.alberto.model.AuditLogStatisticsDao;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuditLogStatisticsOutputToRemoveTest {

    private static final String ACTION = "action";
    private static final String TRACE = "trace";
    private static final String DATE = "2020-01-01";
    private static final long SUM = 1L;

    @Test
    void setActionTest() {
        AuditLogStatisticsDao auditLogStatisticsDao = new AuditLogStatisticsDao();
        auditLogStatisticsDao.setAction(ACTION);
        auditLogStatisticsDao.setTrace(TRACE);
        auditLogStatisticsDao.setDate(DATE);
        auditLogStatisticsDao.setSum(SUM);

        assertEquals(ACTION, auditLogStatisticsDao.getAction());
        assertEquals(TRACE, auditLogStatisticsDao.getTrace());
        assertEquals(DATE, auditLogStatisticsDao.getDate());
        assertEquals(SUM, auditLogStatisticsDao.getSum());
    }
}