package org.example.alberto.mongdb;

import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.example.alberto.transformer.AuditLogMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@AutoConfigureDataMongo
@SpringBootTest(
      properties = "de.flapdoodle.mongodb.embedded.version=4.7.1"
)
@EnableAutoConfiguration()
@DirtiesContext
@Disabled
class CustomAuditLogRepositoryImplTest {

    private static final String AUDIT_LOG = "auditLog";
    @Autowired
    private MongoTemplate mongoTemplate;

    private CustomAuditLogRepository customAuditLogRepository;

    @BeforeEach
    void init(){
        mongoTemplate.getDb().createCollection(AUDIT_LOG);
        mongoTemplate.insert(AuditLogMock.newAuditLogEntity(), AUDIT_LOG);
        customAuditLogRepository = new CustomAuditLogRepositoryImpl(mongoTemplate);
    }

    @AfterEach
    void end(){
        mongoTemplate.dropCollection(AUDIT_LOG);
    }

    @Test
    void saveTest() {

        assertEquals(1, mongoTemplate.findAll(AuditLogEntity.class, "auditLog").size());
    }

    @Test
    void statisticsAuditLogTest() {

        CustomAuditLogRepository customAuditLogRepositoryNew = new CustomAuditLogRepositoryImpl(mongoTemplate);

        List<AuditLogStatisticsDao> statisticsAuditLogs = customAuditLogRepositoryNew.statisticsAuditLog();

        assertEquals(2, statisticsAuditLogs.size());
    }

    @ParameterizedTest
    @CsvSource({"0, 100, 1", "1, 100, 0", "-1, 0, 1"})
    void findAllNew(Integer page, Integer size, Integer expected) {

        List<AuditLogEntity> result = customAuditLogRepository.findAllNew(page, size, "", "");

        assertEquals(expected, result.size());
    }
}