# noinspection YAMLSchemaValidation
openapi: 3.0.3
info:
  title: spring-example
  description: spring-example
  version: 1.0.0
  contact:
    name: Alberto Jose Garcia Gago
    email: albertoggago@gmail.com

servers:
  - url: 'http://127.0.0.1:8081'
  - url: 'http://127.0.0.1:8082'
  - url: 'http://127.0.0.1:8083'
tags:
  - name: greetings
    description: Greetings Message
  - name: customers
    description: Customer Management
  - name: auditLogs
    description: managed AuditLog information
paths:
  /greeting:
    get:
      tags:
        - greetings
      summary: Get a message of welcome
      operationId: greetingGet
      parameters:
        - in: query
          name: name
          schema:
            type: string
            default: "World"
      responses:
        200:
          description: A String
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GreetingDao'
        404:
          description: not found
        default:
          description: Unexpected error
  /customers:
    get:
      tags:
        - customers
      summary: Get a the customers
      operationId: customersAllGet
      parameters:
        - in: query
          name: page
          schema:
            type: integer
            default: 0
        - in: query
          name: size
          schema:
            type: integer
            default: 1
        - in: query
          name: sort
          schema:
            type: string
            default: ''
        - in: query
          name: filter
          schema:
            type: string
            default: ''
      responses:
        200:
          description: Return all Customers
          content:
            application/json:
              schema:
                  type: array
                  items:
                    $ref: '#/components/schemas/CustomerDao'
        404:
          description: not found
        default:
          description: Unexpected error
    post:
      tags:
        - customers
      summary: Insert a new customers
      operationId: customerPost
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CustomerDao'
      responses:
        201:
          description: created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerDao'
        400:
          description: bad request
        422:
          description: error processing bad data
        default:
          description: Unexpected error
    put:
      tags:
        - customers
      summary: Update a customers
      operationId: customerPut
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CustomerDao'
      responses:
        200:
          description: updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerDao'
        400:
          description: bad request
        404:
          description: not found
        422:
          description: error processing bad data
        default:
          description: Unexpected error
    delete:
      tags:
        - customers
      summary: Delete the customers
      operationId: deleteAll
      responses:
        202:
          description: Delete customer
        400:
          description: Error running
        default:
          description: Unexpected error
  /customers/{alias}:
    get:
      tags:
        - customers
      summary: Get a the customers
      operationId: customerByAliasGet
      parameters:
        - in: path
          name: alias
          required: true
          schema:
            type: string
      responses:
        200:
          description: Return all Customers
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CustomerDao'
        400:
          description: Error running
        default:
          description: Unexpected error
    delete:
      tags:
        - customers
      summary: Delete the customers
      operationId: deleteByAliasGet
      parameters:
        - in: path
          name: alias
          required: true
          schema:
            type: string
      responses:
        202:
          description: Delete customer
        400:
          description: Error running
        default:
          description: Unexpected error
  /auditLogs:
    get:
      tags:
        - auditLogs
      summary: Get all audit logs
      operationId: auditLogsAllGet
      parameters:
        - in: query
          name: page
          schema:
            type: integer
            default: 0
        - in: query
          name: size
          schema:
            type: integer
            default: 1
        - in: query
          name: sort
          schema:
            type: string
            default: ''
        - in: query
          name: filter
          schema:
            type: string
            default: ''
      responses:
        200:
          description: Return all AuditLogs
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AuditLogDao'
        404:
          description: not found
        default:
          description: Unexpected error
  /auditLogs/{id}:
    get:
      tags:
        - auditLogs
      summary: Get one auditLogs
      operationId: auditLogById
      parameters:
        - in: path
          name: id
          required: true
          schema:
            type: string
      responses:
        200:
          description: Return the AuditLogId
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AuditLogDao'
        404:
          description: not found
        default:
          description: Unexpected error
  /auditLogs/statistics:
    get:
      tags:
        - auditLogs
      summary: Process Statistics
      operationId: auditLogGetStatistics
      responses:
        200:
          description: Return the Statistics
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AuditLogStatisticsDao'
        404:
          description: not found
        default:
          description: Unexpected error
  /auditLogs/statisticsSaved:
    get:
      tags:
        - auditLogs
      summary: Process Statistics
      operationId: auditLogGetStatisticsSaved
      responses:
        200:
          description: Return the Statistics
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AuditLogStatisticsDao'
        404:
          description: not found
        default:
          description: Unexpected error
  /auditLogs/refreshStatisticsSaved:
    post:
      tags:
        - auditLogs
      summary: Process Statistics
      operationId: auditLogRefreshStatisticsSaved
      responses:
        200:
          description: Return the Statistics
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/AuditLogStatisticsDao'
        404:
          description: not found
        default:
          description: Unexpected error
components:
  schemas:
    GreetingDao:
      type: object
      properties:
        id:
          type: integer
          format: int64
        content:
          type: string
      example:
        id: 999999
        content: The content
    CustomerDao:
      type: object
      properties:
        id:
          type: integer
          format: int64
        alias:
          type: string
        name:
          type: string
        email:
          type: string
        creationDate:
          type: string
        updatedDate:
          type: string
      example:
        id: 10
        alias: alberto
        name: Alberto Gago
        email: albertoggago@gmail.com
        creationDate: 2022-12-24 12:04:47
        updatedDate: 2022-12-24 12:04:47
      required:
        - alias
    ErrorResponseDao:
      type: object
      properties:
        title:
          type: string
        status:
          type: integer
        detail:
          type: string
        instance:
          type: string
    AuditLogDao:
      type: object
      properties:
        id:
          type: string
        action:
          type: string
        trace:
          type: array
          items:
            type: string
        parameters:
          type: object
          additionalProperties:
            type: string
        inputObject:
          type: object
        outputObject:
          type: object
        error:
          type: string
        traceError:
          type: object
          items:
            type: string
        createdDate:
          type: string
        updatedDate:
          type: string
        endDate:
          type: string
      example:
        id: "6337388843ff47075d74e2da"
        action: "GET"
        trace:
          - "CustomerService - list"
          - "END"
        parameters:
          - "page": 1
          - "rows": 100
        inputObject:
          "alias": "Alias"
          "name": "New Name"
          "email": "alberto@ll.xll"
          "_class": "org.example.alberto.dao.CustomerDao"
        outputObject:
          -  "_id": 3
             "alias": "albertoGago"
             "name": "Alberto"
             "email": "albertoggago@gmail.com"
             "creationDate": "2022-09-30 18:41:40.000"
             "updatedDate": "2022-09-30 18:41:40.000"
             "_class": "org.example.alberto.dao.CustomerDao"
          -  "_id": 4
             "alias": "pepe"
             "name": "Pepe"
             "email": "pepe@gmail.com"
             "creationDate": "2022-09-30 18:41:40.000"
             "updatedDate": "2022-09-30 18:41:40.000"
             "_class": "org.example.alberto.dao.CustomerDao"
        error: 401
        traceError:
          - class1
          - class2
        createdDate: "2022-09-30T18:42:16.779Z"
        updatedDate: "2022-09-30T18:42:17.939Z"
    AuditLogStatisticsDao:
      type: object
      properties:
        action:
          type: string
        trace:
          type: string
        date:
          type: string
        sum:
          type: integer
          format: int64
      example:
        type: "GET"
        trace: " a - b - c"
        date: "2022-14-04"
        sum: 1000
  responses:
    NotFound:
      description: Not found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorResponseDao'
