--liquibase formatted sql
--changeSet customer:UpdateCustomersPrev
insert into customer (creation_date, updated_date, email, name, alias)
values (CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'albertoggago@gmail.com', 'Alberto', 'albertoggago');

insert into customer (creation_date, updated_date, email, name, alias)
values (CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'pepe@gmail.com', 'Pepe', 'pepe');