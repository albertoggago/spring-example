--liquibase formatted sql
--changeSet customer:CustomerCreation
  create table customer
  (
  id            bigint auto_increment
  primary key,
  creation_date datetime(6)  not null,
  email         varchar(255) not null,
    name          varchar(255) not null,
            updated_date  datetime(6)  not null,
            constraint UK_crkjmjk1oj8gb6j6t5kt7gcxm
            unique (name)
            );
  insert into customer (creation_date, updated_date, email, name)
  values (CURRENT_TIMESTAMP() , CURRENT_TIMESTAMP(), 'albertoggago@gmail.com', 'Alberto');

  insert into customer (creation_date, updated_date, email, name)
  values (CURRENT_TIMESTAMP() , CURRENT_TIMESTAMP(), 'pepe@gmail.com', 'Pepe');