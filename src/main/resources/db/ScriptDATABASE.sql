## https://linuxize.com/post/how-to-install-mariadb-on-ubuntu-18-04/

CREATE USER IF NOT EXISTS 'dev'@'localhost' IDENTIFIED BY 'bertoatf';
DROP USER 'devxxx';
ALTER USER 'dev'@'localhost'  IDENTIFIED BY 'xxxxnewPassword';

GRANT ALL PRIVILEGES ON *.* TO 'dev'@'localhost';
#GRANT SELECT, INSERT, DELETE ON database_name.* TO database_user@'localhost';
#REVOKE ALL PRIVILEGES ON database_name.* TO 'database_user'@'localhost'

SHOW grants for 'dev'@'localhost';

##SELECT * FROM mysql.user;

show tables;

DROP TABLE customer;

CREATE SCHEMA customer;
DROP SCHEMA customer;
commit;
CREATE DATABASE springexample;
DROP DATABASE springexample;

SELECT * from customer;


create table customer
(
    id            bigint       not null
        primary key,
    creation_date date         null,
    updated_date  date         null,
    email         varchar(255) null,
    name          varchar(255) null
)
    engine = MyISAM;


insert into customer (id, creation_date, updated_date, email, name)
values (1, sysdate(), sysdate(), 'albertoggago@gmail.com', 'Alberto');


commit;



create table customer
(
    id            bigint auto_increment
        primary key,
    alias         varchar(25)  not null,
    creation_date datetime(6)  not null,
    email         varchar(255) not null,
    name          varchar(255) not null,
    updated_date  datetime(6)  not null,
    constraint PKUniqueCustomerAlias
        unique (alias)
);

insert into customer (creation_date, updated_date, email, name, alias)
values (CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'albertoggago@gmail.com', 'Alberto', 'albertoggago');

insert into customer (creation_date, updated_date, email, name, alias)
values (CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 'albertoggago@gmail.com', 'Alberto', 'albertoggago');


drop table DATABASECHANGELOG;
drop table DATABASECHANGELOGLOCK;


select *
from DATABASECHANGELOG;

delete
from DATABASECHANGELOG
where 1 = 1;


delete
from customer
where 1 = 1;


delete
from DATABASECHANGELOGLOCK
where 1 = 1;

delete
from customer
where alias = 'albertoggago2';
