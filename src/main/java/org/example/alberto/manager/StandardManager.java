package org.example.alberto.manager;

import org.springframework.data.domain.Sort;

import java.util.*;

public class StandardManager {
    private StandardManager() {}

    private static final String SEPARATOR_INTERNAL = " ";
    private static final String SEPARATOR_FILTERS = ",";

    protected static final String LIKE = "like";
    protected static final String EQUAL = "=";
    protected static final String GREATER = ">";
    protected static final String LESS = "<";
    protected static final String IS = "is";
    protected static final String IS_NOT = "is_not";
    protected static final String NOT = "not";
    protected static final String DAY = "day";
    protected static final String ACC_EQUAL_STRING = "AccEqualString";
    protected static final String ACC_EQUAL_NUMERIC = "AccEqualNumeric";
    protected static final String ACC_GREATER_NUMERIC = "AccGreaterNumeric";
    protected static final String ACC_LESS_NUMERIC = "AccLessNumeric";
    protected static final String ACC_LIKE = "AccLike";
    protected static final String ACC_DAY = "AccDay";
    protected static final String ACC_IS = "AccIs";
    protected static final String ACC_IS_NOT = "AccIsNot";
    protected static final String ACC_NOT = "AccNot";

    protected static final String NULL = "null";
    public static final int INT_ONE_DAY = 86400000;



    protected static List<Sort.Order> getSortOrder(String sort, List<String> declaredFields, String standardField)
            throws IllegalArgumentException {

        if (sort.isEmpty()) {
            return Collections.singletonList(new Sort.Order(Sort.Direction.DESC, standardField));
        }
        List<Sort.Order> orders = Arrays.stream(sort.split(SEPARATOR_FILTERS)).map(a -> {
            String[] sortSplit = a.trim().split(SEPARATOR_INTERNAL);
            if (sortSplit.length == 1) {
                return new Sort.Order(Sort.Direction.ASC, sortSplit[0]);
            }
            if (sortSplit.length > 2) {
                throw new IllegalArgumentException(
                        "Sort too elements: " + a);
            }
            return new Sort.Order(Sort.Direction.fromString(sortSplit[1]), sortSplit[0]);
        }).toList();
        orders.stream().filter(a -> !declaredFields.contains(a.getProperty()))
                .forEach(a -> {throw new IllegalArgumentException(
                        "Sort element does not exist: " + a.getProperty());});
        return orders;
    }

    public static List<SearchElement> generateSearchExampleToList(String filter,
                                                                  Map<String, Map<String,String>> compToken)
            throws IllegalArgumentException {
        List<SearchElement> searchElements = new ArrayList<>();
        if (filter.isEmpty()){
            return searchElements;
        }

        String[] filters = filter.split(SEPARATOR_FILTERS);

        for (String filterElement : filters) {

            String[] filterComponents = filterElement.trim().split(SEPARATOR_INTERNAL);
            if (filterComponents.length != 3) {
                throw new IllegalArgumentException("Filter Parameter Issue, malformed: " + filterElement);
            }
            if (!compToken.containsKey(filterComponents[0])) {
                throw new IllegalArgumentException("Filter Parameter Issue, row does not exist: " + filterComponents[0]);
            }

            filterComponents[1] = filterComponents[1].toLowerCase();

            if (!compToken.get(filterComponents[0]).containsKey(filterComponents[1])) {
                throw new IllegalArgumentException("Filter Parameter Issue, incompatible comparator: " + filterComponents[0]
                        + " and comparator: "+ filterComponents[1]);
            }

            if ((filterComponents[1].equals(IS) && !filterComponents[2].equalsIgnoreCase(NULL)) ||
                (filterComponents[1].equals(IS_NOT) && !filterComponents[2].equalsIgnoreCase(NULL))){
                throw new IllegalArgumentException("Error is with no null, variable: " + filterComponents[0]
                        + " and comparator: "+ filterComponents[1]);
            }

            searchElements.add(new SearchElement(compToken.get(filterComponents[0]).get(filterComponents[1]),
                    filterComponents[0],
                    filterComponents[2]));
        }
        return searchElements;
    }
}
