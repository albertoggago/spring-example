package org.example.alberto.manager;

public record SearchElement(String type, String element, String value) {
}
