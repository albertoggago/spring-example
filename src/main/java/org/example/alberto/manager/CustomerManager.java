package org.example.alberto.manager;

import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.model.CustomerDao;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.example.alberto.manager.StandardManager.*;

public class CustomerManager  {


    private CustomerManager() {}


    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String ALIAS = "alias";
    private static final String EMAIL = "email";
    private static final String CREATION_DATE = "creationDate";
    private static final String UPDATED_DATE = "updatedDate";

    private static final Map<String, Map<String,String>> COMP_TOKEN = new HashMap<>();
    static {
        COMP_TOKEN.put(NAME, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE));
        COMP_TOKEN.put(EMAIL, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE));
        COMP_TOKEN.put(ALIAS, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE));
        COMP_TOKEN.put(ID, Map.of(EQUAL, ACC_EQUAL_NUMERIC, GREATER, ACC_GREATER_NUMERIC,
                                  LESS, ACC_LESS_NUMERIC));
        COMP_TOKEN.put(CREATION_DATE, Map.of(DAY, ACC_DAY));
        COMP_TOKEN.put(UPDATED_DATE, Map.of(DAY, ACC_DAY));
    }

    public static final List<String> LIST_IGNORED_UPDATED = List.of(CREATION_DATE, UPDATED_DATE, ID);


    private static final List<String> SORT_FIELDS =
            Arrays.stream(CustomerEntity.class.getDeclaredFields()).map(Field::getName).toList();


    public static void verifyInsert(CustomerDao customerDao) throws IllegalArgumentException {
        if (customerDao == null) {
            throw new IllegalArgumentException(StandardTexts.DO_NOT_EXIST);
        }
        if (customerDao.getId() != null) {
            throw new IllegalArgumentException(ID + " " + StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER);
        }
        if (customerDao.getCreationDate() != null) {
            throw new IllegalArgumentException(CREATION_DATE + " " +
                    StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER);
        }
        if (customerDao.getUpdatedDate() != null) {
            throw new IllegalArgumentException(UPDATED_DATE + " " +
                    StandardTexts.SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER);
        }
    }

    public static void updateDatesInsert(CustomerDao customerDao) {
        DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
        dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));
        customerDao.setCreationDate(dateFormat.format(new Date()));
    }

    public static void verifyUpdate(CustomerDao customerDao) throws IllegalArgumentException {
        if (customerDao == null) {
            throw new IllegalArgumentException(StandardTexts.CUSTOMER + " " + StandardTexts.DO_NOT_EXIST);
        }
    }

    public static void updateDatesUpdate(CustomerDao customerDao) {
        DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
        dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));
        customerDao.setUpdatedDate(dateFormat.format(new Date()));
    }

    public static List<Sort.Order> getSortOrder(String sort) throws IllegalArgumentException {
        return StandardManager.getSortOrder(sort, SORT_FIELDS, CREATION_DATE);
    }

    @SuppressWarnings("java:S131")
    public static Specification<CustomerEntity> generateSearchExample(String filter) throws IllegalArgumentException {
        Specification<CustomerEntity> specification = Specification.where(null);
        List<SearchElement> searchElements = StandardManager.generateSearchExampleToList(filter, COMP_TOKEN);

        for (SearchElement searchElement : searchElements) {
            switch (searchElement.type()) {
                case ACC_EQUAL_STRING -> specification =
                        specification.and(byHasEqual(searchElement.element(), searchElement.value()));
                case ACC_EQUAL_NUMERIC -> specification =
                        specification.and(byHasEqual(searchElement.element(), Long.parseLong(searchElement.value())));
                case ACC_GREATER_NUMERIC -> specification =
                        specification.and(byGreater(searchElement.element(), Long.parseLong(searchElement.value())));
                case ACC_LESS_NUMERIC -> specification =
                        specification.and(byLess(searchElement.element(), Long.parseLong(searchElement.value())));
                case ACC_LIKE -> specification =
                        specification.and(byLike(searchElement.element(), searchElement.value()));
                case ACC_DAY -> {
                    DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE_DAY);
                    dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));
                    try {
                        Date date = dateFormat.parse(searchElement.value());
                        Date date1 = new Date(date.getTime() + INT_ONE_DAY);
                        specification =
                                specification.and(byGreaterEqual(searchElement.element(), date));
                        specification =
                                specification.and(byLessEqual(searchElement.element(), date1));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Filter Date Issue, component: "
                                + searchElement.element() + ", date:" + searchElement.value() );
                    }
                }
            }
        }

        return specification;
    }



    private static Specification<CustomerEntity> byHasEqual(String field, Object value) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(field), value);
    }
    private static Specification<CustomerEntity> byGreater(String field, Object value ) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThan(root.get(field), (Comparable<Object>) value);
    }
    private static Specification<CustomerEntity> byLess(String field, Object value) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThan(root.get(field), (Comparable<Object>) value);
    }
    private static Specification<CustomerEntity> byLike(String field, String value) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get(field), "%" + value + "%");
    }
   protected static Specification<CustomerEntity> byGreaterEqual(String field, Object value) {

        return (root, query, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get(field), (Comparable<Object>) value);
    }
    protected static Specification<CustomerEntity> byLessEqual(String field, Object value) {

        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThan(root.get(field), (Comparable<Object>) value);
    }
}