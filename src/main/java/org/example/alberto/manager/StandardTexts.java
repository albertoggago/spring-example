package org.example.alberto.manager;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.FeatureDescriptor;
import java.util.stream.Stream;

public class StandardTexts {

    public static final String DO_NOT_EXIST = "do not exist";
    public static final String SHOULD_NOT_FILL_FOR_CREATE_A_NEW_CUSTOMER = "should not fill for create a new customer";
    public static final String GENERAL_TROW_MESSAGE = "Expected doThing() to throw, but it didn't";
    public static final String NOTHING = "Nothing";
    public static final String CUSTOMER = "Customer";
    public static final String CONTROLLER_LOG_S2 = "Controller: %s, Action: %s";
    public static final String CONTROLLER_LOG_S3 = CONTROLLER_LOG_S2 + ", Param: %s";
    public static final String SERVICE_LOG_S2 = "Service: %s, Action: %s";
    public static final String SERVICE_LOG_S3 = CONTROLLER_LOG_S2 + ", Param: %s";
    public static final String SERVICE_LOG_ERROR_S3 = CONTROLLER_LOG_S2 + ", Error: %s";

    public static final String GET = "GET";
    public static final String PUT = "PUT";
    public static final String POST = "POST";
    public static final String DELETE = "DELETE";
    public static final String END_AUDIT_LOG = "END";
    public static final String THROW_ERROR = "THROW ERROR";
    public static final String SOME_ELEMENT_IS_NOT_VALID = "Some element is not valid";

    public static final String FORMAT_DATE = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String GMT = "GMT+0";
    public static final String FORMAT_DATE_DAY = "yyyy-MM-dd";

    private StandardTexts() {
    }

    public enum Error {
        ERROR_SQL("ERROR SQL"),
        ERROR_BAD_REQUEST("Error Bad Request"),
        ERROR_INFO_WRONG("ERROR INFO NOT valid");

        private final String name;

        Error(String s) {
            name = s;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
                .toArray(String[]::new);
    }
}
