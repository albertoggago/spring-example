package org.example.alberto.manager;

import org.example.alberto.model.AuditLogDao;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.example.alberto.manager.StandardManager.*;

@SuppressWarnings("java:S131")
public class AuditLogManager  {
    private AuditLogManager() {}

    private static final String ID = "id";
    private static final String ACTION = "action";
    private static final String ERROR = "error";
    private static final String END_DATE = "endDate";
    private static final String CREATED_DATE = "createdDate";
    private static final String UPDATED_DATE = "updatedDate";
    private static final String TRACE = "trace";
    private static final String INPUT_OBJECT = "inputObject";
    private static final String OUTPUT_OBJECT = "outputObject";
    private static final String OUTPUT_OBJECT_NAME = "outputObject.name";

    private static final Map<String, Map<String,String>> COMP_TOKEN = new HashMap<>();
    static {
        COMP_TOKEN.put(ID, Map.of(EQUAL, ACC_EQUAL_STRING));
        COMP_TOKEN.put(ACTION, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE, NOT, ACC_NOT));
        COMP_TOKEN.put(TRACE, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE));
        COMP_TOKEN.put(INPUT_OBJECT, Map.of(IS, ACC_IS, IS_NOT, ACC_IS_NOT));
        COMP_TOKEN.put(OUTPUT_OBJECT, Map.of(IS, ACC_IS, IS_NOT, ACC_IS_NOT));
        COMP_TOKEN.put(OUTPUT_OBJECT_NAME, Map.of(LIKE, ACC_LIKE));
        COMP_TOKEN.put(ERROR, Map.of(EQUAL, ACC_EQUAL_STRING, LIKE, ACC_LIKE, IS, ACC_IS, IS_NOT, ACC_IS_NOT));
        COMP_TOKEN.put(END_DATE, Map.of(IS_NOT, ACC_IS_NOT, DAY, ACC_DAY));
        COMP_TOKEN.put(CREATED_DATE, Map.of(IS_NOT, ACC_IS_NOT, DAY, ACC_DAY));
        COMP_TOKEN.put(UPDATED_DATE, Map.of(IS_NOT, ACC_IS_NOT, DAY, ACC_DAY));
    }

    private static final List<String> SORT_FIELDS =
            Arrays.stream(AuditLogDao.class.getDeclaredFields()).map(Field::getName).toList();

    public static List<Sort.Order> getSortOrder(String sort)
    throws IllegalArgumentException {
        return StandardManager.getSortOrder(sort, SORT_FIELDS, CREATED_DATE);
    }

    public static Query generateSearchExample(Query query, String filter)
            throws IllegalArgumentException, InvalidMongoDbApiUsageException {
        List<SearchElement> searchElements =
                StandardManager.generateSearchExampleToList(filter, COMP_TOKEN);

        for (SearchElement searchElement : searchElements) {
            switch (searchElement.type()) {
                case ACC_EQUAL_STRING ->
                        query.addCriteria(Criteria.where(searchElement.element())
                                .is(searchElement.value()));
                case ACC_LIKE ->
                        query.addCriteria(Criteria.where(searchElement.element())
                                .regex(".*" + searchElement.value() + ".*", "i"));
                case ACC_IS ->
                        query.addCriteria(Criteria.where(searchElement.element())
                                .isNull());
                case ACC_IS_NOT ->
                        query.addCriteria(Criteria.where(searchElement.element())
                                .ne(null));
                case ACC_NOT ->
                        query.addCriteria(Criteria.where(searchElement.element())
                                .ne(searchElement.value()));
                case ACC_DAY -> {
                    DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE_DAY);
                    dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));
                    try {
                        Date date = dateFormat.parse(searchElement.value());
                        Date date1 = new Date(date.getTime() + INT_ONE_DAY);
                        query.addCriteria(new Criteria()
                                .andOperator(Criteria.where(searchElement.element()).gt(date),
                                             Criteria.where(searchElement.element()).lt(date1)));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("Filter Date Issue, component: "
                                + searchElement.element() + ", date:" + searchElement.value() );
                    }
                }
           }

        }
        return query;
    }
}
