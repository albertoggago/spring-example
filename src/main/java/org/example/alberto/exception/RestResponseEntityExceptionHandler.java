package org.example.alberto.exception;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.ErrorResponseDao;
import org.hibernate.JDBCException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@Slf4j
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    public static final String CAUSE = "; Cause:";

    @ExceptionHandler({JDBCException.class})
    public ResponseEntity<ErrorResponseDao> jdbcException(
            JDBCException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ErrorResponseDao().title(StandardTexts.Error.ERROR_SQL.toString())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .detail(ex.getMessage() + CAUSE + getFinalCause(ex)));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<ErrorResponseDao> javaxConstraintViolationException(
            ConstraintViolationException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                new ErrorResponseDao().title(StandardTexts.Error.ERROR_INFO_WRONG.toString())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .detail(ex.getMessage() + CAUSE + getFinalCause(ex)));
    }

    public static String getFinalCause(Throwable x) {
        if (x.getCause() == null || x.getCause().getMessage() == null) {
            return x.getMessage();
        } else {
            return getFinalCause(x.getCause());
        }
    }
}