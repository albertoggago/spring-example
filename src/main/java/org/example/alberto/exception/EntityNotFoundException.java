package org.example.alberto.exception;

import lombok.Getter;

@Getter
public class EntityNotFoundException extends Exception {

    private final String idAuditLogEntity;

    public EntityNotFoundException(String s, String idAuditLogEntity) {
        super(s);
        this.idAuditLogEntity = idAuditLogEntity;
    }
}
