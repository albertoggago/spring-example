package org.example.alberto.exception;

import lombok.Getter;
import org.example.alberto.entity.AuditLogEntity;

@Getter
public class BadDataException extends Exception {

    private final String idAuditLogEntity;

    public BadDataException(String s, AuditLogEntity auditLogEntity) {
        super(s);
        this.idAuditLogEntity = auditLogEntity.getId().toHexString();
    }
}
