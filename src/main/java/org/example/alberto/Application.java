package org.example.alberto;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "org.example.alberto.repository")
@Slf4j
@EnableMongoRepositories(basePackages = "org.example.alberto.mongdb")
@EnableWebMvc
public class Application  {

    public static void main(String[] args) {
        log.info("START STARTING APP");
        SpringApplication.run(Application.class, args);
        log.info("END STARTING APP");
    }
}
