package org.example.alberto.serviceimpl;

import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.exception.BadDataException;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.manager.CustomerManager;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.repository.CustomerRepository;
import org.example.alberto.service.AuditLogService;
import org.example.alberto.service.CustomerService;
import org.example.alberto.transformer.CustomerTransformer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;

import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private static final String SERVICE = "CustomerService - ";
    private final CustomerRepository customerRepository;

    private final AuditLogService auditLogService;

    CustomerServiceImpl(CustomerRepository customerRepository, AuditLogService auditLogService) {
        this.customerRepository = customerRepository;
        this.auditLogService = auditLogService;
    }

    @Override
    public List<CustomerDao> list(Integer page, Integer size, String sort, String filter)
            throws EntityNotFoundException, IllegalArgumentException {
        String action = "list";
        log.info(String.format(StandardTexts.SERVICE_LOG_S2, SERVICE, action));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.GET, SERVICE + action, null, null);

        Page<CustomerEntity> customerEntities =
          customerRepository.findAll(CustomerManager.generateSearchExample(filter),
                                     PageRequest.of(page, size, Sort.by(CustomerManager.getSortOrder(sort))));

        if (customerEntities.isEmpty()) {
            auditLogEntity.addTrace(StandardTexts.DO_NOT_EXIST);
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST);
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new EntityNotFoundException(error, auditLogEntity.getId().toHexString());
        }

        List<CustomerDao> customerDaoList = customerEntities.stream()
                .map(CustomerTransformer::transformToDao)
                .toList();
        auditLogService.saveAndTerminate(auditLogEntity, customerDaoList);
        return customerDaoList;
    }

    @Override
    public CustomerDao findCustomerByAlias(String alias) throws EntityNotFoundException {
        String action = "findCustomerByAlias";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action, alias));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.GET, SERVICE + action,
                                                                Map.of("alias", alias), null);

        CustomerEntity customerEntity = customerRepository.findByAlias(alias);

        if (customerEntity == null || customerEntity.getId() == null) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action,
                                         StandardTexts.DO_NOT_EXIST + " :" + alias);
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new EntityNotFoundException(error, auditLogEntity.getId().toHexString());
        }

        CustomerDao customerDaoList = CustomerTransformer.transformToDao(customerEntity);
        auditLogService.saveAndTerminate(auditLogEntity, customerDaoList);
        return customerDaoList;
    }

    @Override
    public CustomerDao createCustomer(CustomerDao customerDao) throws BadDataException {
        String action = "createCustomer";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action,
                ((customerDao == null) ? null : customerDao.toString())));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.POST,
                SERVICE + action, null, customerDao);

        try {
            CustomerManager.verifyInsert(customerDao);
            CustomerManager.updateDatesInsert(customerDao);
            CustomerEntity customerEntity = CustomerTransformer.transformToEntity(customerDao);

            customerRepository.save(customerEntity);
            CustomerDao customerDao1 = CustomerTransformer.transformToDao(customerEntity);
            auditLogService.saveAndTerminate(auditLogEntity, customerDao1);
            return customerDao1;

        } catch (IllegalArgumentException e) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, e.getMessage());
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new BadDataException(error, auditLogEntity);
        } catch (ConstraintViolationException e) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action,
                    StandardTexts.SOME_ELEMENT_IS_NOT_VALID + ", " + e.getMessage());
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new BadDataException(error, auditLogEntity);
        }
    }

    @Override
    public CustomerDao updatedCustomer(CustomerDao customerDao) throws BadDataException, EntityNotFoundException {

        String action = "updatedCustomer";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action, ((customerDao == null) ? null : customerDao.toString())));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.PUT, SERVICE + action, null, customerDao);

        try {
            CustomerManager.verifyUpdate(customerDao);
            CustomerManager.updateDatesUpdate(customerDao);
            CustomerEntity customerEntity = customerRepository.findByAlias(customerDao.getAlias());
            if (customerEntity == null ) {
                String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action,
                        StandardTexts.CUSTOMER + ": " + customerDao.getAlias() + " " + StandardTexts.DO_NOT_EXIST);
                log.error(error);
                auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
                throw new EntityNotFoundException(error, auditLogEntity.getId().toHexString());
            }

            CustomerTransformer.transformToEntity(customerDao, customerEntity, CustomerManager.LIST_IGNORED_UPDATED);
            customerRepository.save(customerEntity);
            CustomerDao customerDao1 = CustomerTransformer.transformToDao(customerEntity);
            auditLogService.saveAndTerminate(auditLogEntity, customerDao1);
            return customerDao1;

        } catch (IllegalArgumentException e) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, e.getMessage());
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new BadDataException(error, auditLogEntity);
        } catch (ConstraintViolationException | TransactionSystemException e) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.SOME_ELEMENT_IS_NOT_VALID + ", " + e.getMessage());
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new BadDataException(error, auditLogEntity);
        }
    }

    @Override
    public void deleteAllCustomer() throws EntityNotFoundException {
        String action = "delete";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action, null));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.DELETE, SERVICE + action, null, null);

        Page<CustomerEntity> customers = customerRepository.findAll(PageRequest.of(0, 1));
        if (customers.isEmpty()) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST);
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new EntityNotFoundException(error, auditLogEntity.getId().toHexString());
        } else {
            customerRepository.deleteAll();
        }

        auditLogService.saveAndTerminate(auditLogEntity, "Number of lines deleted: " + customers.toList().size());
    }

    @Override
    public void deleteOneCustomer(String alias) throws EntityNotFoundException {
        String action = "delete";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action, alias));
        AuditLogEntity auditLogEntity = auditLogService.saveNew(StandardTexts.DELETE, SERVICE + action, Map.of("alias", alias), null);

        CustomerEntity customerEntity = customerRepository.findByAlias(alias);
        if (customerEntity == null) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.CUSTOMER + ": " + alias + " " + StandardTexts.DO_NOT_EXIST);
            log.error(error);
            auditLogService.save(auditLogEntity, StandardTexts.THROW_ERROR);
            throw new EntityNotFoundException(error, auditLogEntity.getId().toHexString());
        } else {
            customerRepository.deleteById(customerEntity.getId());
        }

        auditLogService.saveAndTerminate(auditLogEntity, null);
    }
}