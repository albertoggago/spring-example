package org.example.alberto.serviceimpl;

import org.example.alberto.service.PersonalConfigurationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
class PersonalConfigurationServiceImpl implements PersonalConfigurationService {

    @Value("${alberto.mongodb.test}")
    boolean testMode;


    @Override
    public boolean getTestMode() {
        return this.testMode;
    }

}
