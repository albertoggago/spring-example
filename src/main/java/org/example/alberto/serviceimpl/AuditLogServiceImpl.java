package org.example.alberto.serviceimpl;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.AuditLogDao;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.example.alberto.mongdb.AuditLogRepository;
import org.example.alberto.mongdb.StatisticsRepository;
import org.example.alberto.service.AuditLogService;
import org.example.alberto.service.PersonalConfigurationService;
import org.example.alberto.transformer.AuditLogTransformer;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class AuditLogServiceImpl implements AuditLogService {

    private static final String SERVICE = "AuditLogService - ";
    public static final String ENTITY = "AuditLogEntity";

    private final AuditLogRepository auditLogRepository;

    private final StatisticsRepository statisticsRepository;

    private final PersonalConfigurationService personalConfigurationService;

    AuditLogServiceImpl(AuditLogRepository auditLogRepository, StatisticsRepository statisticsRepository,
                        PersonalConfigurationService personalConfigurationService){
        this.auditLogRepository = auditLogRepository;
        this.statisticsRepository = statisticsRepository;
        this.personalConfigurationService = personalConfigurationService;
    }

    @Override
    public AuditLogEntity saveNew(String action, String trace, Map<String, String> parameters, Object inputObject) {
        String method = "SaveNew";
        AuditLogEntity auditLogEntity = new AuditLogEntity();
        auditLogEntity.setCreatedDate(new Date());

        auditLogEntity.setAction(action);
        if (trace != null) {
            auditLogEntity.addTrace(trace);
        }
        if (parameters != null) {
            auditLogEntity.setParameters(parameters);
        }
        auditLogEntity.setInputObject(inputObject);

        logStandardMethod(method, auditLogEntity);
        auditLogRepository.save(auditLogEntity);
        if (personalConfigurationService.getTestMode()) {
            testFindAuditLogEntity(auditLogEntity, ENTITY + " - " + method);
        }
        return auditLogEntity;
    }

    private void logStandardMethod(String method, AuditLogEntity auditLogEntity) {
        log.info(ENTITY + "{} - {} - {}", method, auditLogEntity.getAction(), auditLogEntity.getCreatedDate());
    }

    private void testFindAuditLogEntity(AuditLogEntity auditLogEntity, String path) {
        AuditLogEntity auditLogEntityNew = auditLogRepository.findById(auditLogEntity.getId().toHexString()).orElse(new AuditLogEntity());
        log.info("TEST - {} - {}", path, auditLogEntityNew.getId());


    }

    @Override
    public void save(AuditLogEntity auditLogEntity, String trace) {
        if (auditLogEntity != null) {
            String method = "save";
            auditLogEntity.addTrace(trace);

            auditLogEntity.setUpdatedDate(new Date());

            logStandardMethod(method, auditLogEntity);
            auditLogRepository.save(auditLogEntity);
            if (personalConfigurationService.getTestMode()) {
                testFindAuditLogEntity(auditLogEntity, ENTITY + " - " + method);
            }
        }
    }

    @Override
    public void saveAndTerminate(AuditLogEntity auditLogEntity, Object outputObject) {
        if (auditLogEntity != null) {
            String method = "saveAndTerminate";
            auditLogEntity.addTrace(StandardTexts.END_AUDIT_LOG);
            auditLogEntity.setOutputObject(outputObject);

            auditLogEntity.setEndDate(new Date());

            logStandardMethod(method, auditLogEntity);
            auditLogRepository.save(auditLogEntity);
            if (personalConfigurationService.getTestMode()) {
                testFindAuditLogEntity(auditLogEntity, ENTITY + " - " + method);
            }
        }
    }


    @Override
    public void saveError(AuditLogEntity auditLogEntity, String error, List<String> traceError) {
        if (auditLogEntity != null) {
            String method = "saveError";

            auditLogEntity.setError(error);

            auditLogEntity.setTraceError(traceError);

            auditLogEntity.setEndDate(new Date());

            logStandardMethod(method, auditLogEntity);
            auditLogRepository.save(auditLogEntity);
            if (personalConfigurationService.getTestMode()) {
                testFindAuditLogEntity(auditLogEntity, ENTITY + " - " + method);
            }
        }
    }

    @Override
    public AuditLogDao findDaoById(String id) throws EntityNotFoundException {
        String action = "findDaoByID";
        log.info(String.format(StandardTexts.SERVICE_LOG_S3, SERVICE, action, id));

        AuditLogEntity auditLogEntity = findEntityById(id);

        if (auditLogEntity.getId() == null) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST + " :" + id);
            log.error(error);
            throw new EntityNotFoundException(error, null);
        }

        return convertToDao(auditLogEntity);

    }

    @Override
    public AuditLogEntity findEntityById(String idCode) {
        String method = "findEntityByID";

        AuditLogEntity auditLogEntity = auditLogRepository.findById(idCode).orElse(new AuditLogEntity());

        logStandardMethod(method, auditLogEntity);
        return auditLogEntity;
    }


    @Override
    public List<AuditLogDao> list(long page, long size, String sort, String filter)
            throws EntityNotFoundException, IllegalArgumentException, InvalidMongoDbApiUsageException {
        String action = "list";
        log.info(String.format(StandardTexts.SERVICE_LOG_S2, SERVICE, action));

        List<AuditLogEntity> auditLogEntities =
                auditLogRepository.findAllNew(page, size, sort, filter);

        if (auditLogEntities.isEmpty()) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST);
            log.error(error);
            throw new EntityNotFoundException(error, null);
        }

        return auditLogEntities.stream()
                .map(this::convertToDao)
                .toList();
    }

    @Override
    public List<AuditLogStatisticsDao> findStatistics() throws EntityNotFoundException {
        String action = "findStatistics";
        log.info(String.format(StandardTexts.SERVICE_LOG_S2, SERVICE, action));

        List<AuditLogStatisticsDao> auditLogStatisticsDao = auditLogRepository.statisticsAuditLog();

        if (auditLogStatisticsDao.isEmpty()) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST);
            log.error(error);
            throw new EntityNotFoundException(error, null);
        }
        return auditLogStatisticsDao;
    }

    @Override
    public List<AuditLogStatisticsDao> findStatisticsSaved() throws EntityNotFoundException {
        String action = "findStatisticsSaved";
        List<AuditLogStatisticsDao> auditLogStatisticsDaoList = statisticsRepository.findAll();

        if (auditLogStatisticsDaoList.isEmpty()) {
            String error = String.format(StandardTexts.SERVICE_LOG_ERROR_S3, SERVICE, action, StandardTexts.DO_NOT_EXIST);
            log.error(error);
            throw new EntityNotFoundException(error, null);
        }
        return auditLogStatisticsDaoList;
    }

    @Override
    public List<AuditLogStatisticsDao> refreshStatisticsSaved() throws EntityNotFoundException {
        statisticsRepository.deleteAll();
        List<AuditLogStatisticsDao> auditLogStatisticsDaoList = findStatistics();
        statisticsRepository.saveAll(auditLogStatisticsDaoList);
        return auditLogStatisticsDaoList;
    }

    private AuditLogDao convertToDao(AuditLogEntity auditLogEntity) {
        AuditLogDao auditLogDao = new AuditLogDao();
        AuditLogTransformer.transformToDao(auditLogEntity, auditLogDao);
        return auditLogDao;
    }

}
