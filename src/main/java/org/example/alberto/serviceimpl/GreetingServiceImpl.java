package org.example.alberto.serviceimpl;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.model.GreetingDao;
import org.example.alberto.service.GreetingService;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
@Slf4j
public class GreetingServiceImpl implements GreetingService {

    private final AtomicLong counter;
    private static final String TEMPLATE= "Hello, %s!";


    public GreetingServiceImpl() {
        this.counter = new AtomicLong();
    }

    public GreetingDao getGreeting(String name) {
        log.info("getGreeting");
        log.debug("getGreeting, name {}", name);
        return new GreetingDao().id(counter.incrementAndGet()).content(String.format(TEMPLATE, name));
    }
}
