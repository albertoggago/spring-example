package org.example.alberto.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Entity
@Table(name = "customer")
@Getter
@Setter
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "alias", unique = true, nullable = false)
    @Size(min = 3, max = 25)
    private String alias;
    @Column(name = "name", nullable = false)
    @Size(min = 3, max = 255)
    private String name;
    @Email(message = "Please provide a valid email")
    @Column(name = "email", nullable = false)
    private String email;
    @Column(name = "creation_date")
    private Timestamp creationDate;
    @Column(name = "updated_date")
    private Timestamp updatedDate;
}