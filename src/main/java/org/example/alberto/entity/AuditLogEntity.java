package org.example.alberto.entity;

import lombok.Data;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.*;

@Generated
@Document(collection = "auditLog")
@Data
@Getter
@Setter
public class AuditLogEntity {

    @MongoId
    private ObjectId id;
    @Field("action")
    private String action;
    @Field("trace")
    private List<String> trace = new ArrayList<>();
    @Field("parameters")
    private Map<String, String> parameters = new HashMap<>();
    @Field("inputObject")
    private Object inputObject;
    @Field("outputObject")
    private Object outputObject;
    @Field("error")
    private String error;
    @Field("traceError")
    private List<String> traceError;
    @Field("createdDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;
    @Field("updatedDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updatedDate;
    @Field("endDate")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date endDate;

    public void addTrace(String newTrace) {
        this.trace.add(newTrace);
    }
}
