package org.example.alberto.service;

import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.AuditLogDao;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public interface AuditLogService {

    AuditLogEntity saveNew(String action, String trace, Map<String, String> parameters, Object inputObject);

    void save(AuditLogEntity auditLogEntity, String trace);

    void saveError(AuditLogEntity auditLogEntity, String error, List<String> traceError);

    void saveAndTerminate(AuditLogEntity auditLogEntity, Object outputObject);

    AuditLogDao findDaoById(String string) throws EntityNotFoundException;

    AuditLogEntity findEntityById(String string);

    List<AuditLogDao> list(long page, long size, String sort, String filter) throws EntityNotFoundException;

    List<AuditLogStatisticsDao> findStatistics() throws EntityNotFoundException;

    List<AuditLogStatisticsDao> findStatisticsSaved() throws EntityNotFoundException;

    List<AuditLogStatisticsDao> refreshStatisticsSaved() throws EntityNotFoundException;
}
