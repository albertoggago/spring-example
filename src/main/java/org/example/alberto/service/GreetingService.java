package org.example.alberto.service;

import org.example.alberto.model.GreetingDao;

public interface GreetingService {
    GreetingDao getGreeting(String name);
}
