package org.example.alberto.service;

import org.example.alberto.exception.BadDataException;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.CustomerDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {


    List<CustomerDao> list(Integer page, Integer size, String sort, String filter)
            throws EntityNotFoundException, IllegalArgumentException;

    CustomerDao findCustomerByAlias(String name) throws EntityNotFoundException;

    CustomerDao createCustomer(CustomerDao customerEntity) throws BadDataException;

    CustomerDao updatedCustomer(CustomerDao customerDao) throws BadDataException, EntityNotFoundException;

    void deleteAllCustomer() throws EntityNotFoundException;

    void deleteOneCustomer(String alias) throws EntityNotFoundException;
}
