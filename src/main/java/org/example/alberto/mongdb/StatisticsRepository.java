package org.example.alberto.mongdb;

import org.example.alberto.model.AuditLogStatisticsDao;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatisticsRepository extends MongoRepository<AuditLogStatisticsDao, String> {
}
