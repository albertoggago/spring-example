package org.example.alberto.mongdb;

import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.manager.AuditLogManager;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.DateOperators;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class CustomAuditLogRepositoryImpl implements CustomAuditLogRepository {

    public static final String TRACE = "trace";
    public static final String ACTION = "action";
    public static final String CREATED_DATE = "createdDate";
    public static final String CREATED_DATE_SORT = "createdDateSort";
    private static final String DATE = "date";
    private static final String SUM = "sum";
    private static final String COLLECTION = "auditLog";
    private final MongoTemplate mongoTemplate;

    CustomAuditLogRepositoryImpl(MongoTemplate mongoTemplate){
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<AuditLogStatisticsDao> statisticsAuditLog() {

        Aggregation aggregation = newAggregation(
                project(fields(TRACE, ACTION)).andExclude(Fields.UNDERSCORE_ID)
                        .and(DateOperators.dateOf(CREATED_DATE).toString("%Y-%m-%d")).as(CREATED_DATE_SORT)
                , unwind(TRACE)
                , group(fields().and(TRACE).and(ACTION).and(DATE, "$createdDateSort")).count().as(SUM)
                , project(SUM).andExclude(Fields.UNDERSCORE_ID).andExpression("_id.trace").as(TRACE)
                        .andExpression("_id.date").as(DATE)
                        .andExpression("_id.action").as(ACTION)
                , sort(Sort.Direction.DESC, SUM)
        );
        AggregationResults<AuditLogStatisticsDao> auditLogEntities = mongoTemplate.aggregate(aggregation, COLLECTION, AuditLogStatisticsDao.class);
        return auditLogEntities.getMappedResults();
    }

    @Override
    public List<AuditLogEntity> findAllNew(long page, long size, String sort, String filter)
    throws IllegalArgumentException, InvalidMongoDbApiUsageException {
        Query query = new Query();
        query.with(Sort.by(AuditLogManager.getSortOrder(sort)));


        return mongoTemplate.find(AuditLogManager.generateSearchExample(query, filter)
                                , AuditLogEntity.class)
                .stream()
                .skip((page < 0 ? 0 : page) * (size < 1 ? 1 : size))
                .limit( size < 1 ? 1 : size)
                .toList();
    }
}
