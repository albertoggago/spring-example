package org.example.alberto.mongdb;

import org.example.alberto.entity.AuditLogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuditLogRepository extends MongoRepository<AuditLogEntity, String>, CustomAuditLogRepository {
}
