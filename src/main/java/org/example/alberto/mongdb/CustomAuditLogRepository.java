package org.example.alberto.mongdb;

import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface CustomAuditLogRepository {
    List<AuditLogStatisticsDao> statisticsAuditLog();
    List<AuditLogEntity> findAllNew(long page, long size, String sort, String filter)
            throws IllegalArgumentException;
}
