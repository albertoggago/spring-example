package org.example.alberto.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.api.GreetingApiDelegate;
import org.example.alberto.model.GreetingDao;
import org.example.alberto.service.GreetingService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class GreetingController implements GreetingApiDelegate {

    private final GreetingService greetingService;

    GreetingController(GreetingService greetingService){
        this.greetingService = greetingService;
    }

    @Override
    public ResponseEntity<GreetingDao> greetingGet(String body) {
        return ResponseEntity.ok(greetingService.getGreeting(body));
    }
}
