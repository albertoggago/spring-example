package org.example.alberto.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.api.AuditLogsApiDelegate;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.model.AuditLogDao;
import org.example.alberto.model.AuditLogStatisticsDao;
import org.example.alberto.service.AuditLogService;
import org.springframework.data.mongodb.InvalidMongoDbApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@Component
@Slf4j
public class AuditLogController implements AuditLogsApiDelegate {
    private static final String CONTROLLER = "AuditLogControllerController";

    private final AuditLogService auditLogService;

    AuditLogController(AuditLogService auditLogService){
        this.auditLogService = auditLogService;
    }

    @Override
    public ResponseEntity<List<AuditLogDao>> auditLogsAllGet(Integer page, Integer size, String sort, String filter) {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S2, CONTROLLER, StandardTexts.GET));
        try {
            return ResponseEntity.ok(auditLogService.list(page, size, sort, filter));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }  catch (IllegalArgumentException | InvalidMongoDbApiUsageException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<AuditLogDao> auditLogById(String id) {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S2, CONTROLLER, StandardTexts.GET));
        try {
            return ResponseEntity.ok(auditLogService.findDaoById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<AuditLogStatisticsDao>> auditLogGetStatistics() {
        try {
            return ResponseEntity.ok(auditLogService.findStatistics());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<AuditLogStatisticsDao>> auditLogGetStatisticsSaved() {
        try {
            return ResponseEntity.ok(auditLogService.findStatisticsSaved());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<AuditLogStatisticsDao>> auditLogRefreshStatisticsSaved() {
        try {
            return ResponseEntity.ok(auditLogService.refreshStatisticsSaved());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
}
