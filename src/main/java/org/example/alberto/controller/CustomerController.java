package org.example.alberto.controller;

import lombok.extern.slf4j.Slf4j;
import org.example.alberto.api.CustomersApiDelegate;
import org.example.alberto.exception.BadDataException;
import org.example.alberto.exception.EntityNotFoundException;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@Slf4j
public class CustomerController implements CustomersApiDelegate {

    private static final String CONTROLLER = "CustomerController";

    private final CustomerService customerService;

    CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }


    @Override
    public ResponseEntity<List<CustomerDao>> customersAllGet(Integer page,
                                                             Integer size,
                                                             String sort,
                                                             String filter)  {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S2, CONTROLLER, StandardTexts.GET));
        try {
            return ResponseEntity.ok(customerService.list(page, size, sort, filter));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<CustomerDao> customerByAliasGet(String alias)  {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S3, CONTROLLER, StandardTexts.GET, alias));
        try {
            return ResponseEntity.ok(customerService.findCustomerByAlias(alias));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<CustomerDao> customerPost(CustomerDao customerDao) {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S3, CONTROLLER, StandardTexts.POST, customerDao.toString()));
        try {
            return new ResponseEntity<>(customerService.createCustomer(customerDao), HttpStatus.CREATED);
        } catch (BadDataException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage());
        }
    }

    @Override
    public ResponseEntity<CustomerDao> customerPut(CustomerDao customerDao) {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S3, CONTROLLER, StandardTexts.PUT, customerDao.toString()));
        try {
            return ResponseEntity.ok(customerService.updatedCustomer(customerDao));
        } catch (BadDataException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());        }
    }

    @Override
    public ResponseEntity<Void> deleteAll() {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S2, CONTROLLER, StandardTexts.DELETE));
        try {
            customerService.deleteAllCustomer();
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @Override
    public ResponseEntity<Void> deleteByAliasGet(String alias) {
        log.info(String.format(StandardTexts.CONTROLLER_LOG_S3, CONTROLLER, StandardTexts.DELETE, alias));
        try {
            customerService.deleteOneCustomer(alias);
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
