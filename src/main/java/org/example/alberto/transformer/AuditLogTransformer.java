package org.example.alberto.transformer;

import org.bson.types.ObjectId;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.entity.AuditLogEntity;
import org.example.alberto.model.AuditLogDao;
import org.springframework.beans.BeanUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class AuditLogTransformer {

    private AuditLogTransformer() {

    }

    public static void transformToDao(AuditLogEntity auditLogEntity, AuditLogDao auditLogDao) {

        if (auditLogEntity != null) {
            BeanUtils.copyProperties(auditLogEntity, auditLogDao);

            DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
            dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));

            auditLogDao.setId(auditLogEntity.getId().toHexString());

            if (auditLogEntity.getCreatedDate() != null) {
                auditLogDao.setCreatedDate(dateFormat.format(auditLogEntity.getCreatedDate()));
            }
            if (auditLogEntity.getUpdatedDate() != null) {
                auditLogDao.setUpdatedDate(dateFormat.format(auditLogEntity.getUpdatedDate()));
            }
            if (auditLogEntity.getEndDate() != null) {
                auditLogDao.setEndDate(dateFormat.format(auditLogEntity.getEndDate()));
            }
        }
    }

    public static void transformToEntity(AuditLogDao auditLogDao, AuditLogEntity auditLogEntity) {
        transformToEntity(auditLogDao, auditLogEntity, new String[0]);
    }

    public static void transformToEntity(AuditLogDao auditLogDao, AuditLogEntity auditLogEntity, String[] excluded) {

        if (auditLogDao != null) {
            BeanUtils.copyProperties(auditLogDao, auditLogEntity, excluded);

            DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
            dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));

            auditLogEntity.setId(new ObjectId(auditLogDao.getId()));

            auditLogEntity.setCreatedDate(getCreatedDate(dateFormat, String.valueOf(auditLogDao.getCreatedDate())));
            auditLogEntity.setUpdatedDate(getCreatedDate(dateFormat, String.valueOf(auditLogDao.getUpdatedDate())));
            auditLogEntity.setEndDate(getCreatedDate(dateFormat, String.valueOf(auditLogDao.getEndDate())));
        }
    }

    private static Timestamp getCreatedDate(DateFormat dateFormat, String date) {
        try {
            return new Timestamp(dateFormat.parse(date).getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    public static void transformToEntityNotNulls(AuditLogDao auditLogDao, AuditLogEntity auditLogEntity) {
        transformToEntity(auditLogDao, auditLogEntity, StandardTexts.getNullPropertyNames(auditLogDao));
    }
}
