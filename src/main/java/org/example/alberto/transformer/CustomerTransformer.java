package org.example.alberto.transformer;


import org.example.alberto.entity.CustomerEntity;
import org.example.alberto.manager.StandardTexts;
import org.example.alberto.model.CustomerDao;
import org.springframework.beans.BeanUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

public class CustomerTransformer {

    private CustomerTransformer() {
    }

    public static CustomerDao transformToDao(CustomerEntity customerEntity) {
        return transformToDao(customerEntity, null);
    }

    public static CustomerDao transformToDao(CustomerEntity customerEntity, CustomerDao customerDao) {

        if (customerDao == null) {
            customerDao = new CustomerDao();
        }
        if (customerEntity != null) {
            BeanUtils.copyProperties(customerEntity, customerDao);

            DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
            dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));

            if (customerEntity.getCreationDate() != null) {
                customerDao.setCreationDate(dateFormat.format(customerEntity.getCreationDate()));
            }
            if (customerEntity.getUpdatedDate() != null) {
                customerDao.setUpdatedDate(dateFormat.format(customerEntity.getUpdatedDate()));
            }
        }
        return customerDao;
    }

    public static CustomerEntity transformToEntity(CustomerDao customerDao) {
        return transformToEntity(customerDao, null, Collections.emptyList());
    }
    public static CustomerEntity transformToEntity(CustomerDao customerDao, CustomerEntity customerEntity) {
        return transformToEntity(customerDao, customerEntity, Collections.emptyList());
    }

    public static CustomerEntity transformToEntity(CustomerDao customerDao, CustomerEntity customerEntity, List<String> excluded) {

        if (customerEntity == null) {
            customerEntity = new CustomerEntity();
        }
        if (customerDao != null) {
            BeanUtils.copyProperties(customerDao, customerEntity, excluded.toArray(String[]::new));

            DateFormat dateFormat = new SimpleDateFormat(StandardTexts.FORMAT_DATE);
            dateFormat.setTimeZone(TimeZone.getTimeZone(StandardTexts.GMT));

            if (customerDao.getCreationDate() != null) {
                try {
                    customerEntity.setCreationDate(new Timestamp(dateFormat.parse(customerDao.getCreationDate()).getTime()));
                } catch (ParseException e) {
                    customerEntity.setCreationDate(null);
                }
            }
            if (customerDao.getUpdatedDate() != null) {
                try {
                    customerEntity.setUpdatedDate(new Timestamp(dateFormat.parse(customerDao.getUpdatedDate()).getTime()));
                } catch (ParseException e) {
                    customerEntity.setUpdatedDate(null);
                }
            }
        }
        return customerEntity;
    }
}