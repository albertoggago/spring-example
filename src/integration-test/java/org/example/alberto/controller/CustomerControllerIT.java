package org.example.alberto.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.alberto.model.CustomerDao;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;


import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@AutoConfigureDataMongo
@SpringBootTest()
@DirtiesContext
@Sql(scripts = {"/initSql.sql"})
@Disabled
class CustomerControllerIT {

    private static final String AUDIT_LOG = "auditLog";
    @Autowired
    MockMvc mockMvc;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    CustomerController customerController;

    @BeforeEach
    void init(){
        mongoTemplate.getDb().createCollection(AUDIT_LOG);
    }
    @AfterEach
    void end(){
        mongoTemplate.dropCollection(AUDIT_LOG);
    }

    @Test
    @Order(1)
    void getCustomersTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$[0].name", is(CustomerMock.NAME)))
                .andExpect(jsonPath("$[0].email", is(CustomerMock.EMAIL)))
                .andExpect(jsonPath("$[0].id", notNullValue()))
                .andExpect(jsonPath("$[0].creationDate", notNullValue()))
                .andExpect(jsonPath("$[0].updatedDate", nullValue()));
    }
    @Test
    @Order(2)
    void postCustomerTest() throws Exception {
        CustomerDao customerDao = new CustomerDao().alias("2ONE").name("Name2").email("name2@test.com");

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(customerDao))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    @Test
    @Order(3)
    void postCustomerBadResponse() throws Exception {

        CustomerDao customerDao = new CustomerDao().alias("2ONE").name("Name2").email("name2test.com");

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.detail", containsString("Controller: CustomerService - , Action: createCustomer, Error: Some element is not valid")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    @Order(4)
    void putCustomerTest() throws Exception {

        String newName = "NewName";

        CustomerDao customerDao = CustomerMock.createCustomerDaoInsert();
        customerController.customerPost(CustomerMock.createCustomerDaoInsert());
        customerDao.setName(newName);

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.alias", is(CustomerMock.ALIAS)))
                .andExpect(jsonPath("$.name", is(newName)));
    }

    @Test
    @Order(5)
    void putCustomerTestErrorNotFound() throws Exception {

        CustomerDao customerDao = CustomerMock.newCustomerDao();

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is("Controller: CustomerService - , Action: updatedCustomer, Error: Customer: alias do not exist")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    @Order(6)
    void putCustomerTestErrorNotValid() throws Exception {

        String newEmail = "NewName";

        CustomerDao customerDao = CustomerMock.createCustomerDaoInsert();
        customerController.customerPost(customerDao);
        customerDao.setEmail(newEmail);


        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customerDao))
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.UNPROCESSABLE_ENTITY.value())))
                .andExpect(jsonPath("$.detail", containsString("Controller: CustomerService - , Action: updatedCustomer, Error: Some element is not valid")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }


   @Test
   @Order(7)
    void deleteAllCustomerTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.ACCEPTED.value()));
    }

    @Test
    @Order(8)
    void deleteAllCustomerTestNotFound() throws Exception {

        String uri = "/customers";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    @Order(9)
    void deleteOneCustomerTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/customers/" + CustomerMock.ALIAS;
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.ACCEPTED.value()));
    }

    @Test
    @Order(10)
    void deleteOneCustomerTestNotFound() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/customers/" + "OtherCustomer";
        mockMvc.perform(MockMvcRequestBuilders.delete(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
