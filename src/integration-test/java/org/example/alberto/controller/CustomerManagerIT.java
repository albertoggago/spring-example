package org.example.alberto.controller;

import org.example.alberto.model.CustomerDao;
import org.example.alberto.transformer.CustomerMock;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc
@AutoConfigureDataMongo
@SpringBootTest()
@DirtiesContext
@Sql(scripts = {"/initSql.sql"})
@Disabled
class CustomerManagerIT {

    private static final String AUDIT_LOG = "auditLog";
    @Autowired
    MockMvc mockMvc;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    CustomerController customerController;

    private static final String C2_ALIAS = "alias2";
    private static final String C2_NAME = "alias2";
    private static final String C2_EMAIL = "email2@gmail.com";

    private CustomerDao customer1;
    private CustomerDao customer2;
    @BeforeEach
    void init(){
        mongoTemplate.getDb().createCollection(AUDIT_LOG);

        customer1 = customerController.customerPost(CustomerMock.createCustomerDaoInsert()).getBody();

        CustomerDao customerNew = CustomerMock.createCustomerDaoInsert();
        customerNew.setCreationDate(null);
        customerNew.setUpdatedDate(null);
        customerNew.setAlias(C2_ALIAS);
        customerNew.setName(C2_NAME);
        customerNew.setEmail(C2_EMAIL);
        customer2 = customerController.customerPost(customerNew).getBody();
    }
    @AfterEach
    void end(){
        mongoTemplate.dropCollection(AUDIT_LOG);
    }

    @Test
    @Order(1)
    void getCustomersTestTwoCustomersSorting() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/customers?size=100&sort=alias ASC")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(customer1.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer1.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer1.getName())))
                .andExpect(jsonPath("$[0].email", is(customer1.getEmail())))
                .andExpect(jsonPath("$[1].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[1].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[1].name", is(customer2.getName())))
                .andExpect(jsonPath("$[1].email", is(customer2.getEmail())));
    }
    @ParameterizedTest
    @CsvSource(value = {"alias#=#"+C2_ALIAS, "name#=#"+C2_NAME, "email#=#"+C2_EMAIL,
                         "name#like#2", "alias#like#2", "email#like#2" }
             , delimiter= '#')
    @Order(2)
    void getCustomersTestTwoCustomersStringEqualLike(String name, String compare, String value) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/customers?size=100&filter=" + name + " " + compare + " " + value)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer2.getName())))
                .andExpect(jsonPath("$[0].email", is(customer2.getEmail())));
    }

    @Test
    @Order(3)
    void getCustomersTestTwoCustomersID() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/customers?size=100&filter= id = " + customer2.getId())
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer2.getName())))
                .andExpect(jsonPath("$[0].email", is(customer2.getEmail())));
    }

    @Test
    @Order(4)
    void getCustomersTestTwoCustomersCreationDate() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders
                .get("/customers?size=100&sort=alias DESC&filter= creationDate day "
                                + customer2.getCreationDate().substring(0, 10)
                        )
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer2.getName())))
                .andExpect(jsonPath("$[0].email", is(customer2.getEmail())))
                .andExpect(jsonPath("$[0].creationDate", is(customer2.getCreationDate())))
                .andExpect(jsonPath("$[0].updatedDate", nullValue()))
                .andExpect(jsonPath("$[1].id", is(customer1.getId()), Long.class))
                .andExpect(jsonPath("$[1].alias", is(customer1.getAlias())))
                .andExpect(jsonPath("$[1].name", is(customer1.getName())))
                .andExpect(jsonPath("$[1].email", is(customer1.getEmail())))
                .andExpect(jsonPath("$[1].creationDate", is(customer1.getCreationDate())))
                .andExpect(jsonPath("$[1].updatedDate", nullValue()));
    }

    @Test
    @Order(5)
    void getCustomersTestTwoCustomersUpdatedDate() throws Exception {

        customer2.setName("new name");
        customer2 = customerController.customerPut(customer2).getBody();

        assert customer2 != null;
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/customers?size=100&filter= updatedDate day "
                                + customer2.getUpdatedDate().substring(0, 10)
                        )
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[0].name", is("new name")))
                .andExpect(jsonPath("$[0].email", is(customer2.getEmail())))
                .andExpect(jsonPath("$[0].creationDate", is(customer2.getCreationDate())))
                .andExpect(jsonPath("$[0].updatedDate", is(customer2.getUpdatedDate())));
    }

    @Test
    @Order(6)
    void getCustomersTestTwoCustomersGreaterId() throws Exception {

        long number = customer2.getId() -1;
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/customers?size=100&filter= id > "
                                + number
                        )
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(customer2.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer2.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer2.getName())))
                .andExpect(jsonPath("$[0].email", is(customer2.getEmail())));
    }

    @Test
    @Order(6)
    void getCustomersTestTwoCustomersLessId() throws Exception {

        long number = customer1.getId()  + 1;
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/customers?size=100&filter= id < "
                                + number
                        )
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(customer1.getId()), Long.class))
                .andExpect(jsonPath("$[0].alias", is(customer1.getAlias())))
                .andExpect(jsonPath("$[0].name", is(customer1.getName())))
                .andExpect(jsonPath("$[0].email", is(customer1.getEmail())));
    }
}
