package org.example.alberto.controller;

import org.example.alberto.transformer.CustomerMock;
import org.json.JSONArray;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@AutoConfigureDataMongo
@SpringBootTest()
@DirtiesContext
@Sql(scripts = {"/initSql.sql"})
@Disabled
class AuditLogControllerIT {

    private static final String AUDIT_LOG = "auditLog";
    @Autowired
    MockMvc mockMvc;

    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    CustomerController customerController;

    @BeforeEach
    void init(){
        mongoTemplate.getDb().createCollection(AUDIT_LOG);
    }
    @AfterEach
    void end(){
        mongoTemplate.dropCollection(AUDIT_LOG);
    }

    @Test
    @Order(1)
    void getAuditLogsTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/auditLogs";
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", not(emptyString())))
                .andExpect(jsonPath("$[0].action", is("POST")))
                .andExpect(jsonPath("$[0].trace.[0]",  is("CustomerService - createCustomer")))
                .andExpect(jsonPath("$[0].trace.[1]",  is("END")))
                .andExpect(jsonPath("$[0].parameters", anEmptyMap()))
                .andExpect(jsonPath("$[0].inputObject", not(anEmptyMap())))
                .andExpect(jsonPath("$[0].outputObject", not(anEmptyMap())))
                .andExpect(jsonPath("$[0].error", nullValue()))
                .andExpect(jsonPath("$[0].traceError", nullValue()))
                .andExpect(jsonPath("$[0].createdDate", not(nullValue())))
                .andExpect(jsonPath("$[0].endDate", not(nullValue())))
                .andExpect(jsonPath("$[0].updatedDate", nullValue()))
                ;
    }

    @Test
    @Order(2)
    void getAuditLogsFailed() throws Exception {

        String uri = "/auditLogs";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is("Controller: AuditLogService - , Action: list, Error: do not exist")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

    @Test
    @Order(3)
    void getOneAuditLogTest() throws Exception {


        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/auditLogs";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        JSONArray json = new JSONArray(mvcResult.getResponse().getContentAsString());

        uri = "/auditLogs/" + json.getJSONObject(0).get("id");
        mockMvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", not(emptyString())))
                .andExpect(jsonPath("$.action", is("POST")))
                .andExpect(jsonPath("$.trace.[0]",  is("CustomerService - createCustomer")))
                .andExpect(jsonPath("$.trace.[1]",  is("END")))
                .andExpect(jsonPath("$.parameters", anEmptyMap()))
                .andExpect(jsonPath("$.inputObject", not(anEmptyMap())))
                .andExpect(jsonPath("$.outputObject", not(anEmptyMap())))
                .andExpect(jsonPath("$.error", nullValue()))
                .andExpect(jsonPath("$.traceError", nullValue()))
                .andExpect(jsonPath("$.createdDate", not(nullValue())))
                .andExpect(jsonPath("$.endDate", not(nullValue())))
                .andExpect(jsonPath("$.updatedDate", nullValue()));
    }

    @Test
    @Order(4)
    void getOneAuditLogFailedTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/auditLogs/" + "NO-ID";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath("$.status", is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath("$.detail", is("Controller: AuditLogService - , Action: findDaoByID, Error: do not exist :NO-ID")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }


    @Test
    @Order(5)
    void getStatisticsOkTest() throws Exception {

        customerController.customerPost(CustomerMock.createCustomerDaoInsert());

        String uri = "/auditLogs/statistics";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].action", is("POST")))
                .andExpect(jsonPath("$[1].action", is("POST")))
                .andExpect(jsonPath("$[0].trace", oneOf("END", "CustomerService - createCustomer")))
                .andExpect(jsonPath("$[1].trace", oneOf("END", "CustomerService - createCustomer")))
                .andExpect(jsonPath("$[0].sum", is(1)))
                .andExpect(jsonPath("$[1].sum", is(1)))
                .andExpect(jsonPath("$[0].date", not(nullValue())))
                .andExpect(jsonPath("$[0].date", not(nullValue())));
    }

    @Test
    @Order(6)
    void getStatisticsFailedTest() throws Exception {

        String uri = "/auditLogs/statistics";
        mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath("$.title", is("Not Found")))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.detail", is("Controller: AuditLogService - , Action: findStatistics, Error: do not exist")))
                .andExpect(jsonPath("$.instance", is(uri)));
    }

}
