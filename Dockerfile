FROM maven:3.9.9 AS MAVEN-BUILDER
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package  -Dmaven.test.skip=true

FROM openjdk:21 AS openjdk

RUN addgroup -S nonroot \
    && adduser -S nonroot -G nonroot

USER nonroot

COPY --from=MAVEN-BUILDER /home/app/target/*.jar /usr/local/lib/spring-example.jar
ENTRYPOINT ["java", "-jar", "/usr/local/lib/spring-example.jar"]