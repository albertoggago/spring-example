# Spring boot application

this app is a test to add all techniques into a Rest full Spring boot

## Technologies

* Spring Boot
* Rest Full
* MariaDB + Hibernate
* Swagger

You can use any configuration of Maria DB, MongoDB or Jenkins
Or use:
[Docker-tools](https://gitlab.com/albertoggago/docker-tools)

## Urls to help:

### DOCKER:

* https://medium.com/@HoussemDellai/setup-sonarqube-in-a-docker-container-3c3908b624df

### Spring

* https://www.baeldung.com/hibernate-exceptions
* http://logback.qos.ch/manual/layouts.html
* https://www.baeldung.com/spring-data-mongodb-tutorial
* https://spring.io/guides/gs/accessing-data-mongodb/
* https://www.baeldung.com/spring-data-mongodb-index-annotations-converter
* https://www.baeldung.com/mockito-void-methods

### CI

The project was tested in JenkinsFile

And GitLab