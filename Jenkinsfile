pipeline{
    agent any
    tools {
        maven 'Maven'
        jdk 'jdk'
    }
    stages {
        stage('Initialize') {
            steps {
                sh '''
                echo "PATH = ${PATH}"
                 echo "M2_HOME = ${M2_HOME}"
                '''
            }
        }
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Build') {
            steps {
                sh 'mvn -Dmaven.test.failure.ignore=true clean package'
            }
            post {
                success {
                    junit 'target/surefire-reports/**/*.xml'
                }
            }
        }

        stage('SonarQube') {
            steps {
                withSonarQubeEnv(installationName: 'SonarQubeServer') {
                    echo "Static code analysis with SonarQube runner"
                    sh "mvn sonar:sonar -Dsonar.projectKey=spring-example"
                }
            }
        }

        stage("Quality Gate") {
            steps {
                timeout(time: 3, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
      stage('Archive') {
        steps {
          archiveArtifacts artifacts: '**/target/*.war, **/target/*.zip, **/target/*.jar', onlyIfSuccessful: true, fingerprint: true, allowEmptyArchive: true
        }
      }
    }
}
